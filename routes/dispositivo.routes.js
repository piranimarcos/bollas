const express = require("express");
const router = express.Router();
const controller = require("../controllers/dispositivo.controller");


router.get("/ultima-medicion", controller.getUltimasMediciones);
router.get("/rotacion", controller.getRotacion);


module.exports = router;
