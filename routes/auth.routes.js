const controller = require("../controllers/auth.controller");
const { check } = require('express-validator');

module.exports = function (app) {

  app.post("/api/auth/signin", controller.signin);


  // app.get("/api/auth/jwt", authJwt.verifyTokenExpiration, (req, res) => {
  app.get("/api/auth/jwt", (req, res) => {
    return res.status(200).send({ ok: true });
  });

};