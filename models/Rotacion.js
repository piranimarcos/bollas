var mongoose = require('mongoose');

var rotacionSchema = new mongoose.Schema({
  idDispositivo: {type: mongoose.Schema.Types.ObjectId, ref: 'dispositivo', required: true},
  x: {type: Number, required: true},
  y: {type: Number, required: true},
  alerta: {type: Boolean, required: true, default: false},
  alertaVista: {type: Boolean, required: true, default: false}
}, { timestamps: true });

var Rotacion = mongoose.model('rotacion', rotacionSchema);

module.exports = Rotacion;
