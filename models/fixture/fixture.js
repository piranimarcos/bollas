module.exports.sensores = [
  // {
  //   nombre: "Rotación en x",
  //   nombreRef: "rotacionX",
  //   tipo: "analogico",
  //   condiciones: [],
  //   unidad: ''
  // },
  // {
  //   nombre: "Rotación en y",
  //   nombreRef: "rotacionY",
  //   tipo: "analogico",
  //   condiciones: [],
  //   unidad: ''
  // },
  {
    nombre: "Corriente en la baliza",
    nombreRef: "corrienteBaliza",
    tipo: "analogico",
    condiciones: [
      { condicion: '=', valor: '0' }
    ],
    unidad: 'mA'
  },
  {
    nombre: "Nivel del rio bajo",
    nombreRef: "nivelRioBajo",
    tipo: "digital",
    condiciones: [
      { condicion: '=', valor: '0' }
    ],
    unidad: ''
  },
  {
    nombre: "Nivel del rio alto",
    nombreRef: "nivelRioAlto",
    tipo: "digital",
    condiciones: [
      { condicion: '=', valor: '1' }
    ],
    unidad: ''
  },
  {
    nombre: "Estado de la boya",
    nombreRef: "estadoBoya",
    tipo: "digital",
    condiciones: [
      { condicion: '=', valor: '0' }
    ],
    unidad: ''
  },

  {
    nombre: "Estado del sensor mpu",
    nombreRef: "estadoMpu",
    tipo: "digital",
    condiciones: [
      { condicion: '=', valor: '0' }
    ],
    unidad: ''
  },

  {
    nombre: "Estado del sensor ina",
    nombreRef: "estadoIna",
    tipo: "digital",
    condiciones: [
      { condicion: '=', valor: '0' }
    ],
    unidad: ''
  },

  {
    nombre: "Estado del flotador",
    nombreRef: "estadoFlotador",
    tipo: "digital",
    condiciones: [
      { condicion: '=', valor: '0' }
    ],
    unidad: ''
  },

  {
    nombre: "Estado del sensor de impacto",
    nombreRef: "estadoImpacto",
    tipo: "digital",
    condiciones: [
      { condicion: '=', valor: '0' }
    ],
    unidad: ''
  },

  {
    nombre: "Voltaje en la batería del cpu",
    nombreRef: "voltajeBateriaArduino",
    tipo: "analogico",
    condiciones: [
      { condicion: '<', valor: '3.2' }
    ],
    unidad: 'V'
  },

  {
    nombre: "Estado de la bateria del cpu",
    nombreRef: "estadoBateriaArduino",
    tipo: "digital",
    condiciones: [
      { condicion: '=', valor: '0' }
    ],
    unidad: ''
  },

  {
    nombre: "Voltaje en la bateria del gps",
    nombreRef: "voltajeBateriaGps",
    tipo: "analogico",
    condiciones: [
      { condicion: '<', valor: '3.2' }
    ],
    unidad: 'V'
  },
  {
    nombre: "Estado de la bateria del gps",
    nombreRef: "estadoBateriaGps",
    tipo: "digital",
    condiciones: [
      { condicion: '=', valor: '0' }
    ],
    unidad: ''
  }
]


// db.sensors.update({ 'tipo': 'digital' }, { $set: { 'condiciones': [ { condicion: '=', valor: '0' }, { condicion: '=', valor: 'false' } ] } }, { multi: true })

// db.dispositivos.update({ 'llave': '321321' }, { $set: { 'posicion': { lat: -33.223690, lng: -60.314878 } } } )
