const mongoose = require('mongoose');
const Dispositivo = require('../Dispositivo');
const Sensor = require('../Sensor');
const Evento = require('../Evento');
const Medicion = require('../Medicion');
const User = require('../User');



const sensores = require('./fixture').sensores

sensores.map(sen => {
  console.log(sen.condiciones)
})


mongoose.connect('mongodb://localhost/bollas', { useUnifiedTopology: true, useNewUrlParser: true });
mongoose.set('useCreateIndex', true)
// get reference to database
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', function () {
  console.log("Connection Successful!");

  // define Schema
  let BookSchema = mongoose.Schema({
    name: String,
    price: Number,
    quantity: Number
  });

  // a document instance
  let user = new User({
    name: "Admin",
    email: "a@a.com",
    password: "123123"
  });

  // creo
  user.save(function (err, user) {

    console.log(user.name + " usuario guardado.");

    User.findOne({ name: user.name }, function (err, docUser) {
      if (err) return console.error(err);

      console.log(docUser);

      let dispositivo1 = new Dispositivo({
        nombre: "Boya 1",
        descripcion: "Boya de señalización primaria",
        posicion: {
          lat: -33.228776,
          lng: -60.337536
        },
        distanciaCritica: 0.3,
        usuarioReferencia: docUser._id,
        llave: "123123"
      });

      let dispositivo2 = new Dispositivo({
        nombre: "Boya 2",
        descripcion: "Boya de señalización secundaria",
        posicion: {
          lat: -33.233690,
          lng: -60.334878
        },
        distanciaCritica: 0.3,
        usuarioReferencia: docUser._id,
        llave: "321321"
      });

      dispositivo1.save(function (err, disp) {
        console.log(disp.nombre + " dispositivo guardado.");

        Dispositivo.findOne({ nombre: disp.nombre }, function (err, docDisp) {
          if (err) return console.error(err);

          console.log(docDisp);

          sensores.map( dat => {
            console.log(dat)
            let sensor = new Sensor({
              idDispositivo: docDisp._id,
              nombre: dat.nombre,
              tipo: dat.tipo,
              nombreRef: dat.nombreRef,
              condiciones: dat.condiciones,
              unidad: dat.unidad,
              descripcion: dat.nombre
            });

            sensor.save(function (err, sens) {
              console.log(sens.nombre + " sensor guardado.");
            })

          });

        });

      });


      dispositivo2.save(function (err, disp2) {
        console.log(disp2.nombre + " dispositivo guardado.");

        Dispositivo.findOne({ nombre: disp2.nombre }, function (err, docDisp2) {
          if (err) return console.error(err);

          console.log(docDisp2);

          sensores.map( dat => {
            console.log(dat)
            let sensor = new Sensor({
              idDispositivo: docDisp2._id,
              nombre: dat.nombre,
              tipo: dat.tipo,
              nombreRef: dat.nombreRef,
              condiciones: dat.condiciones,
              unidad: dat.unidad,
              descripcion: dat.nombre
            });

            sensor.save(function (err, sens) {
              console.log(sens.nombre + " sensor guardado.");
            })

          });

        });

      });

    });

  });

});
