var mongoose = require('mongoose');

var dispositivoSchema = new mongoose.Schema({
  nombre: {type: String, required: true},
  descripcion: {type: String, required: true},
  posicion:{
   lat: {type: Number},
   lng: {type: Number}
  },
  distanciaCritica: {type: Number, required: true},
  usuarioReferencia: {type: mongoose.Schema.Types.ObjectId, ref: 'user', required: true},
  llave: { type: String, required: true }
}, { timestamps: true });

var Dispositivo = mongoose.model('dispositivo', dispositivoSchema);

module.exports = Dispositivo;
