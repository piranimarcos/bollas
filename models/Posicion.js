var mongoose = require('mongoose');

var posicionSchema = new mongoose.Schema({
  idDispositivo: {type: mongoose.Schema.Types.ObjectId, ref: 'dispositivo', required: true},
  lat: {type: String, required: true},
  lng: {type: String, required: true},
  distancia: {type: Number, required: true},
  alerta: {type: Boolean, required: true, default: false},
  alertaVista: {type: Boolean, required: true, default: false}
}, { timestamps: true });

var Posicion = mongoose.model('posicion', posicionSchema);

module.exports = Posicion;
