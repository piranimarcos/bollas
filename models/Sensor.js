var mongoose = require('mongoose');

var sensorSchema = new mongoose.Schema({
  idDispositivo: {type: mongoose.Schema.Types.ObjectId, ref: 'dispositivo', required: true},
  nombre: {type: String, required: true},
  nombreRef: {type: String, required: true},
  descripcion: {type: String, required: true},
  tipo: {type: String, required: true, enum:['analogico', 'digital'], default: 'analogico'},
  unidad: {type: String, default: ''},
  condiciones: [
    {
      condicion: {type: String, enum: ['<', '=', '>'] },
      valor: {type: String}
    }
  ]
}, { timestamps: true });

var Sensor = mongoose.model('sensor', sensorSchema);

module.exports = Sensor;
