var mongoose = require('mongoose');

var eventoSchema = new mongoose.Schema({
  idSensor: {type: mongoose.Schema.Types.ObjectId, ref: 'sensor', required: true},
  condiciones: [
    {
      condicion: {type: String, enum: ['<', '=', '>'], required: true},
      valor: {type: String, required: true}
    }
  ]
}, { timestamps: true });

var Evento = mongoose.model('evento', eventoSchema);

module.exports = Evento;
