var mongoose = require('mongoose');

var medicionSchema = new mongoose.Schema({
  idSensor: {type: mongoose.Schema.Types.ObjectId, ref: 'sensor', required: true},
  valor: {type: String, required: true},
  alerta: {type: Boolean, required: true, default: false},
  alertaVista: {type: Boolean, required: true, default: false}
}, { timestamps: true });

var Medicion = mongoose.model('medicion', medicionSchema);

module.exports = Medicion;
