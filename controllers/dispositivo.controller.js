const mongoose = require('mongoose');
const Dispositivo = require('../models/Dispositivo');
const Sensor = require('../models/Sensor');
const Evento = require('../models/Evento');
const Posicion = require('../models/Posicion');
const Medicion = require('../models/Medicion');
const async = require('async')

exports.getUltimasMediciones = (req, res) => {
    let mediciones = []

    if (!req.query.id)
        return res.json({ error: 'no envio id' })

    let id = req.query.id

    Sensor.find({ idDispositivo: id })
        .populate({
            path: 'idDispositivo'
        })
        .exec(function (err, sensores) {
            if (err)
                return res.json({ error: 'no hay sensores' })

            async.groupBy(sensores, function (sensor, callback) {
                Medicion.findOne({ idSensor: sensor._id }).sort('-createdAt').populate({ path: 'idSensor' }).exec((err, medicion) => {
                    if (err) return callback(err);

                    console.log(sensor.nombreRef);
                    mediciones.push(medicion)
                    return callback(null, medicion);
                })
            }, function (err, result) {
                if (err) return res.json({ error: err });

                let nombres = []
                mediciones.map((med, i) => {
                    if (med)
                        nombres.push(sensores[i])
                })
                return res.json({mediciones: mediciones, cantidad: mediciones.length })
            });
        })

};


exports.getRotacion = (req, res) => {

    if (!req.query.rotacionX || !req.query.rotacionY)
        return res.json({ error: 'no envio rotacionX o rotacionY' })

    let rotacionX = [], rotacionY = []

    let condiciones = [{ idSensor: req.query.rotacionX }, { idSensor: req.query.rotacionY }]


    Medicion.find({ $or: condiciones }).populate({ path: 'idSensor' }).exec((err, mediciones) => {
        if (err)
            return res.json({ error: 'no hay mediciones' })

        mediciones.map(med => {
            if (med.idSensor.nombreRef == 'rotacionX')
                rotacionX.push(med)
            if (med.idSensor.nombreRef == 'rotacionY')
                rotacionY.push(med)
        })
        return res.json({ rotacionX: rotacionX, rotacionY: rotacionY })
    })

};
