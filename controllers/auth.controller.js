const { validationResult } = require('express-validator');

const jwt = require("jsonwebtoken");

function tokenForUser(user) {
  return jwt.sign({ id: user.id }, config.secret, {
    expiresIn: 86400  // 24 hours
  });
}


exports.signin = (req, res) => {

  if(!req.body.username || !req.body.password){
    return res.status(400).send({ error: 'ingrese datos' });
  }

  if (req.body.username === 'admin' && req.body.password === '123123')
    return res.status(200).send({ message: 'Ok', token: 'entro', id: '1' });
  return res.status(401).send({ error: 'usuario incorrecto' });
};