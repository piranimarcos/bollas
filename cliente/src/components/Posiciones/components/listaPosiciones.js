import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Circle, Map, TileLayer, Marker, Popup } from 'react-leaflet'
import L from 'leaflet';
import moment from 'moment'


import History from '../../../history.js';


class ListaPosiciones extends Component {


  constructor(props) {
    super(props)

    this.state = { posiciones: [], dispositivo: {}, center: [], zoom: 14 }
  }

  componentWillReceiveProps(props) {
    if (props.posiciones) {
      let posiciones = props.posiciones
      posiciones = posiciones.map(posicion => {
        posicion.createdAt = moment(posicion.createdAt).format('YYYY/MM/DD hh:mm:ss')
        return posicion
      })
      this.setState({ posiciones: posiciones })
    }
    if (props.dispositivo && props.dispositivo.length > 0) {
      let disp = props.dispositivo[0]
      this.setState({ dispositivo: disp, center: [disp.posicion.lat, disp.posicion.lng] })
    }
  }



  render() {
    let posiciones = this.state.posiciones
    let dispositivo = this.state.dispositivo
    let center = this.state.center

    let greenIcon = new L.Icon({
      iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png',
      shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
      iconSize: [25, 41],
      iconAnchor: [12, 41],
      popupAnchor: [1, -34],
      shadowSize: [41, 41]
    });

    let redIcon = new L.Icon({
      iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-red.png',
      shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
      iconSize: [25, 41],
      iconAnchor: [12, 41],
      popupAnchor: [1, -34],
      shadowSize: [41, 41]
    });


    if (posiciones.length > 0 && center.length > 0)
      return (
        <Map center={center} zoom={this.state.zoom}>
          <TileLayer
            attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
          <Marker position={center}>
            <Popup>
              {dispositivo.nombre}
            </Popup>
          </Marker>
          <Circle center={center} fillColor="green" color="green" radius={300} >
            <Popup>Radio límite</Popup>
          </Circle>
          {this.state.posiciones.map((posicion, i) => {
            let pos = [posicion.lat, posicion.lng]
            if (posicion.alerta)
              return (
                <Marker key={posicion._id} position={pos} icon={redIcon}>
                  <Popup>
                    <div className="alert alert-danger">

                      <h5><i className="icon fas fa-ban" /> Alert!</h5>
                      {posicion.createdAt} <br />
                      {posicion.lat} | {posicion.lng}

                    </div>

                  </Popup>
                </Marker>
              )

              return (
                <Marker key={posicion._id} position={pos} icon={greenIcon}>
                  <Popup>
                    <div className="alert alert-success">

                      <h5><i className="icon fas fa-check" /> Ok!</h5>
                      {posicion.createdAt} <br />
                      {posicion.lat} | {posicion.lng}

                    </div>

                  </Popup>
                </Marker>
              )
          })}
        </Map>
      )

    return (
      <div>
        <p>No hay datos</p>
        <button className="btn btn-default" onClick={History.goBack}>Volver</button>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {}
};



export default connect(mapStateToProps, {})(ListaPosiciones);
