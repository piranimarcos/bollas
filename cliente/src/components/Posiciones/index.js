import React, { Component } from 'react';
import { connect } from 'react-redux'


import { ToastContainer, toast } from 'react-toastify'

import 'react-toastify/dist/ReactToastify.css';
import ListaPosiciones from './components/listaPosiciones';

import { fetchPosicions } from '../../actions/posiciones';
import { getDispositivo } from '../../actions/dispositivos';

class Posiciones extends Component {

  renderError() {
    if (this.props.error)
      toast.error(this.props.error, { position: "top-right", autoClose: 5000, hideProgressBar: false, closeOnClick: true, pauseOnHover: true, draggable: true })


    if (this.props.message)
      toast.success(this.props.message, { position: "top-right", autoClose: 5000, hideProgressBar: false, closeOnClick: true, pauseOnHover: true, draggable: true });

  }

  componentWillMount() {
    let dispositivo = this.props.match.params.dispositivo;
    this.props.dispatch(fetchPosicions(dispositivo));
    this.props.dispatch(getDispositivo(dispositivo));
  }

  renderLoading() {
    return <div className="overlay"><i className="fas fa-2x fa-sync-alt fa-spin" /></div>
  }


  render() {

    return (
      <div>

        {this.renderError()}

        <ListaPosiciones dispositivo={this.props.dispositivos} posiciones={this.props.posicions} />

        <ToastContainer />
      </div>
    );

  }
}

const mapStateToProps = (state) => {
  return {
    dispositivos: state.dispositivo.dispositivos,
    posicions: state.posicion.posicions,
    error: state.dispositivo.error,
    message: state.dispositivo.message
  }
};


const mapDispatchToProps = () => {
  return {

  }
}



export default connect(mapStateToProps, mapDispatchToProps)(Posiciones);
