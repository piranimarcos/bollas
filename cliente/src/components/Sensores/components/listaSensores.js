import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';


class ListaSensores extends Component {

  constructor(props) {
    super(props)

    this.state = { sensores: [], dispositivos: [], rotacionX: '', rotacionY: '' }
  }

  componentWillReceiveProps(props) {
    if (props.sensores) {
      let sensores = props.sensores

      this.setState({ sensores: props.sensores }, () => console.log(this.state))
    }
    if (props.dispositivos) {
      this.setState({ dispositivos: props.dispositivos })
    }
  }


  render() {

    return (
      <div className="row">


        {this.state.dispositivos.map(dispositivo => {
          return (
            <div className="col-sm-6">

              <div className="card card-default" key={dispositivo._id}>

                <div className="card-header">
                  <h3 className="card-title"> <i className="fas fa-bullhorn" /> {dispositivo.nombre} </h3>
                </div>

                <div className="card-body">

                  <NavLink exact to={`/dashboard/dispositivos/rotacion/${dispositivo._id}`}>

                    <div className="callout callout-info">
                      <p key="1">Oleaje</p>
                    </div>
                  </NavLink>


                  {this.state.sensores.map(sensor => {
                    if (sensor.idDispositivo === dispositivo._id)

                    if(sensor.nombreRef != 'rotacionX' && sensor.nombreRef != 'rotacionY')
                      return (
                        <NavLink exact to={`/dashboard/sensores/${sensor._id}`}>
                          <div className="callout callout-info">
                            <p key={sensor._id}>{sensor.nombre}</p>
                          </div>
                        </NavLink>
                      )
                  })}

                </div>

              </div>
            </div>
          )
        })}



      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {}
};

const removeItemFromArr = (arr, item) => {
  let i = arr.indexOf(item);

  if (i !== -1) {
    arr.splice(i, 1);
  }
}

export default connect(mapStateToProps, {})(ListaSensores);
