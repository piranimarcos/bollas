import React, { Component } from 'react';
import { connect } from 'react-redux'


import {ToastContainer ,toast} from 'react-toastify'
import ListaSensores from './components/listaSensores'

import 'react-toastify/dist/ReactToastify.css';

import { fetchSensors } from '../../actions/sensores';
import { fetchDispositivos } from '../../actions/dispositivos';

class Sensores extends Component {

  renderError() {
    if (this.props.error)
      toast.error(this.props.error, { position: "top-right", autoClose: 5000, hideProgressBar: false, closeOnClick: true, pauseOnHover: true, draggable: true })


    if (this.props.message)
      toast.success(this.props.message, { position: "top-right", autoClose: 5000, hideProgressBar: false, closeOnClick: true, pauseOnHover: true, draggable: true });
    
  }

  renderLoading() {
    return <div className="overlay"><i className="fas fa-2x fa-sync-alt fa-spin" /></div>
  }
  
  componentWillMount(){
    this.props.dispatch(fetchSensors())
    this.props.dispatch(fetchDispositivos())
  }


  render() {

    return (
      <div>

        {this.renderError()}
        
        <ListaSensores sensores={this.props.sensores} dispositivos={this.props.dispositivos}/>
       
        <ToastContainer />

      </div>
    );

  }
}

const mapStateToProps = (state) => {
  return {
    sensores: state.sensor.sensors,
    dispositivos: state.dispositivo.dispositivos,
    error: state.sensor.error,
    message: state.sensor.message
  }
};

const mapDispatchToProps  = () => {
  return {
    fetchSensors,
    fetchDispositivos
  }
}



export default connect(mapStateToProps, mapDispatchToProps)(Sensores);
