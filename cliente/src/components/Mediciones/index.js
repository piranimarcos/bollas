import React, { Component } from 'react';
import { connect } from 'react-redux'


import {ToastContainer ,toast} from 'react-toastify'
import ListaMediciones from './components/listaMediciones'

import 'react-toastify/dist/ReactToastify.css';

import { fetchMedicions, clearMsj } from '../../actions/mediciones';
import { getSensor } from '../../actions/sensores';

class Mediciones extends Component {

  constructor(props) {
    super(props)

    this.state = { nombre: [] }
  }


  renderError() {
    if (this.props.error)
      toast.error(this.props.error, { position: "top-right", autoClose: 5000, hideProgressBar: false, closeOnClick: true, pauseOnHover: true, draggable: true })


    if (this.props.message)
      toast.success(this.props.message, { position: "top-right", autoClose: 5000, hideProgressBar: false, closeOnClick: true, pauseOnHover: true, draggable: true });
    
  }

  renderLoading() {
    return <div className="overlay"><i className="fas fa-2x fa-sync-alt fa-spin" /></div>
  }


  
  componentWillMount(){
    let sensor = this.props.match.params.sensor;
    this.props.dispatch(clearMsj())
    this.props.dispatch(getSensor(sensor))
    this.props.dispatch(fetchMedicions(sensor))
  }


  render() {

    return (
      <div className="row">

        {this.renderError()}
        
        <ListaMediciones mediciones={this.props.mediciones} sensor={this.props.sensores}/>
       
        <ToastContainer />

      </div>
    );

  }
}

const mapStateToProps = (state) => {
  return {
    mediciones: state.medicion.medicions,
    sensores: state.sensor.sensors,
    error: state.medicion.error,
    message: state.medicion.message
  }
};

const mapDispatchToProps  = () => {
  return {
    fetchMedicions,
    getSensor
  }
}



export default connect(mapStateToProps, mapDispatchToProps)(Mediciones);
