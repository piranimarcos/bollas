import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import moment from 'moment'

import LineChart from './lineChart';

import History from '../../../history.js';


import socketIOClient from "socket.io-client";
import { SOCKET } from '../../../routes/config'

class ListaMediciones extends Component {

  constructor(props) {
    super(props)

    this.state = { mediciones: [], sensor: [] }
  }

  componentWillReceiveProps(props) {
    if (props.mediciones) {
      let mediciones = props.mediciones
      mediciones = mediciones.map(medicion => {
        medicion.createdAt = moment(medicion.createdAt).format('DD/MM/YYYY HH:mm:ss')
        return medicion
      })
      this.setState({ mediciones: mediciones })
    }
    if (props.sensor) {
      this.setState({ sensor: props.sensor })
    }
  }

  componentDidMount() {
    const socket = socketIOClient(SOCKET);
    socket.on("nuevaMedicion", (data , nombre) => {
      let medicion = JSON.parse(data)
      console.log('nueva Medicion -->' + JSON.parse(data))

      if(this.state.sensor[0]._id == medicion.idSensor){
        console.log('hay coincidencia')
        medicion.createdAt = moment(medicion.createdAt).format('DD/MM/YYYY HH:mm:ss')
        let mediciones = this.state.mediciones
        mediciones.unshift(medicion)
        this.setState({mediciones: mediciones})
      }
    });
  }

  render() {
    let mediciones = this.state.mediciones
    let nombre = 'cargando'

    if (mediciones.length > 0) {
      mediciones = mediciones.slice(0, 99)
    }
    if (this.state.sensor.length > 0)
      nombre = this.state.sensor[0].nombre

    if (mediciones.length > 0)
      return (
        <div className="col-sm-12">

          <div className="card card-primary">
            <div className="card-header">
              <h3 className="card-title">{nombre}</h3>
              <div className="card-tools">
                <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus" />
                </button>
                <button type="button" className="btn btn-tool" data-card-widget="remove"><i className="fas fa-times" /></button>
              </div>
            </div>
            <div className="card-body">
              {(this.state.sensor.length > 0) ? <LineChart data={mediciones} sensor={this.state.sensor[0]} title={nombre} color="#70CAD1" /> : ''}
            </div>
          </div>




          <div className="card card-primary">
            <div className="card-header">
              <h3 className="card-title">Historial de mediciones</h3>
            </div>
            {/* /.card-header */}
            <div className="card-body table-responsive p-0">
              <table className="table table-hover text-nowrap">
                <thead>
                  <tr>
                    <th>Hora</th>
                    <th>Valor</th>
                    <th>Alerta</th>
                  </tr>
                </thead>
                <tbody>

                  {this.state.mediciones.map(medicion => {
                    return (
                      <tr key={medicion._id} className={medicion.alerta ? "table-danger" : "table-success"}>
                        <td>{medicion.createdAt}</td>
                        <td>{medicion.valor}</td>
                        <td>{medicion.alerta}</td>
                      </tr>
                    )

                  })}
                </tbody>
              </table>
            </div>
            {/* /.card-body */}
          </div>


          <hr />


        </div >
      )
    return (
      <div>
        <p>No hay mediciones</p>
        <button className="btn btn-default" onClick={History.goBack}>Volver</button>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {}
};



export default connect(mapStateToProps, {})(ListaMediciones);
