import React, { Component } from 'react';
import Chart from "chart.js";

class LineChart extends Component {
  constructor(props) {
    super(props);
    this.chartRef = React.createRef();
  }

  componentDidUpdate() {
    this.myChart.data.labels = this.props.data.map(d => d.createdAt);
    this.myChart.data.datasets[0].data = this.props.data.map(d => d.valor);
    this.myChart.update();
  }

  componentDidMount() {
    let datos = []
    let sensor = this.props.sensor


    sensor.condiciones.map(cond => {
      datos.push({
        label: `alerta ${cond.condicion} ${cond.valor}`,
        data: this.props.data.map(d => parseFloat(cond.valor)),
        fill: 'none',
        backgroundColor: 'red',
        pointRadius: 2,
        borderColor: 'red',
        borderWidth: 1,
        lineTension: 0
      })
    })

    datos.push({
      label: this.props.title,
      data: this.props.data.map(d => parseFloat(d.valor)),
      fill: '',
      backgroundColor: this.props.color,
      pointRadius: 2,
      borderColor: this.props.color,
      borderWidth: 3,
      lineTension: 0
    })

    this.myChart = new Chart(this.chartRef.current, {
      type: 'line',
      options: {
        tooltips: {
          enabled: true,
          mode: 'single',
          callbacks: {
            label: (item) => {
              return `${item.yLabel} ${sensor.unidad}`;
            }
          }
        },
        scales: {
          yAxes: [{
            stacked: false,
            ticks: {
              // Include a dollar sign in the ticks
              callback: function (value, index, values) {
                return value + sensor.unidad;
              }
            }
          }]
        },
      },
      data: {
        labels: this.props.data.map(d => `${d.createdAt}`),
        datasets: datos
      }
    });

  }

  render() {
    return <canvas ref={this.chartRef} />;
  }
}

export default LineChart