import React, { Component } from 'react';
import { connect } from 'react-redux'


import { ToastContainer, toast } from 'react-toastify'
import ListaGaleria from './components/listaGaleria'

import 'react-toastify/dist/ReactToastify.css';
import { FOTOS } from '../../routes/config';


class Galeria extends Component {
  constructor() {
    super()
    this.state = { fotos: [] }
  }

  renderError() {
    if (this.props.error)
      toast.error(this.props.error, { position: "top-right", autoClose: 5000, hideProgressBar: false, closeOnClick: true, pauseOnHover: true, draggable: true })


    if (this.props.message)
      toast.success(this.props.message, { position: "top-right", autoClose: 5000, hideProgressBar: false, closeOnClick: true, pauseOnHover: true, draggable: true });

  }

  renderLoading() {
    return <div className="overlay"><i className="fas fa-2x fa-sync-alt fa-spin" /></div>
  }

  componentWillMount() {
    fetch(FOTOS + 'fotos-nombre')
      .then(response => response.json())
      .then(data => {
        this.setState({ fotos: data }, () => console.log(this.state))
      });
  }


  render() {

    // let fotos = [{nombre:"15-08-2020 17:00:45.jpg"},{nombre:"15-08-2020 17:02:13.jpg"},{nombre:"15-08-2020 17:02:49.jpg"},{nombre:"15-08-2020 17:20:30.jpg"},{nombre:"15-08-2020 18:25:38.jpg"},{nombre:"15-08-2020 18:28:43.jpg"},{nombre:"15-08-2020 18:41:53.jpg"},{nombre:"15-08-2020 18:42:21.jpg"},{nombre:"15-08-2020 18:42:26.jpg"},{nombre:"15-08-2020 18:53:07.jpg"},{nombre:"15-08-2020 18:56:51.jpg"},{nombre:"15-08-2020 19:03:56.jpg"},{nombre:"29-07-2020 16:34:44.jpg"},{nombre:"29-07-2020 17:11:51.jpg"},{nombre:"29-07-2020 17:12:39.jpg"},{nombre:"29-07-2020 17:14:34.jpg"},{nombre:"29-07-2020 17:15:15.jpg"},{nombre:"29-07-2020 17:16:14.jpg"},{nombre:"29-07-2020 17:17:38.jpg"},{nombre:"29-07-2020 17:55:39.jpg"},{nombre:"29-07-2020 17:56:09.jpg"},{nombre:"29-07-2020 18:03:14.jpg"},{nombre:"29-07-2020 18:11:07.jpg"},{nombre:"29-07-2020 18:11:27.jpg"},{nombre:"29-07-2020 20:17:57.jpg"},{nombre:"29-07-2020 20:25:59.jpg"},{nombre:"29-07-2020 20:26:21.jpg"}]

    let fotos = this.state.fotos
    if (fotos.length > 0)
      return (
        <div className="row">

          {this.renderError()}

          <ListaGaleria fotos={fotos} />

          <ToastContainer />

        </div>
      );

    return(<div></div>)
  }
}

const mapStateToProps = (state) => {
  return {
    sensores: state.sensor.sensors,
    dispositivos: state.dispositivo.dispositivos,
    error: state.sensor.error,
    message: state.sensor.message
  }
};

const mapDispatchToProps = () => {
  return {

  }
}



export default connect(mapStateToProps, mapDispatchToProps)(Galeria);
