import React, { Component } from 'react';
import { connect } from 'react-redux';

import { FOTOS } from '../../../routes/config'


class ListaGaleria extends Component {

  constructor(props) {
    super(props)

    this.state = { fotoSeleccionada: this.props.fotos[0].nombre }
  }

  render() {

    return (
      <div>


        <div className="col-md-12">
          <div className="card card-primary">
            <div className="card-header">
              <div className="card-title">
                Último dato ({this.state.fotoSeleccionada})
    </div>
            </div>
            <div className="card-body text-center">

              <a href={FOTOS + "fotos?img=" + this.state.fotoSeleccionada} data-toggle="lightbox" data-title="sample 1 - white" data-gallery="gallery">
                <img src={FOTOS + "fotos?width=500&img=" + this.state.fotoSeleccionada} className="img-responsive" />
              </a>
            </div>
          </div>
        </div>

        <div className="col-md-12">
          <div className="card card-primary">
            <div className="card-header">
              <div className="card-title">
                Fotos
      </div>
            </div>
            <div className="card-body">
              <div className="row">
                {
                  this.props.fotos.map(foto => {
                    return (
                      <div className="col-sm-2">

                        <a onClick={() => {
                          this.setState({ fotoSeleccionada: foto.nombre })
                          window.scrollTo(0, 0)
                        }}>
                          <img src={FOTOS + "fotos?width=100&img=" + foto.nombre} className="img-fluid mb-2" />
                        </a>
                      </div>
                    )
                  })
                }
              </div>
            </div>
          </div>
        </div>


      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {}
};



export default connect(mapStateToProps, {})(ListaGaleria);
