import React, { Component } from 'react';
import { connect } from 'react-redux'


import { ToastContainer, toast } from 'react-toastify'

import 'react-toastify/dist/ReactToastify.css';
import ListaRotaciones from './components/listaRotaciones';

import { fetchRotacions } from '../../actions/rotaciones';
import { getDispositivo } from '../../actions/dispositivos';

class Rotaciones extends Component {

  renderError() {
    if (this.props.error)
      toast.error(this.props.error, { position: "top-right", autoClose: 5000, hideProgressBar: false, closeOnClick: true, pauseOnHover: true, draggable: true })


    if (this.props.message)
      toast.success(this.props.message, { position: "top-right", autoClose: 5000, hideProgressBar: false, closeOnClick: true, pauseOnHover: true, draggable: true });

  }

  componentWillMount() {
    let dispositivo = this.props.match.params.dispositivo;
    this.props.dispatch(fetchRotacions(dispositivo));
    this.props.dispatch(getDispositivo(dispositivo));
  }

  renderLoading() {
    return <div className="overlay"><i className="fas fa-2x fa-sync-alt fa-spin" /></div>
  }


  render() {

    return (
      <div>

        {this.renderError()}

        <ListaRotaciones dispositivo={this.props.dispositivos} rotaciones={this.props.rotacions} />

        <ToastContainer />
      </div>
    );

  }
}

const mapStateToProps = (state) => {
  return {
    dispositivos: state.dispositivo.dispositivos,
    rotacions: state.rotacion.rotacions,
    error: state.dispositivo.error,
    message: state.dispositivo.message
  }
};


const mapDispatchToProps = () => {
  return {

  }
}



export default connect(mapStateToProps, mapDispatchToProps)(Rotaciones);
