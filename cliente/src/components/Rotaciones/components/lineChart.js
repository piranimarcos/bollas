import React, { Component } from 'react';
import Chart from "chart.js";

class LineChart extends Component {
  constructor(props) {
    super(props);
    this.chartRef = React.createRef();
  }

  componentDidUpdate() {
    this.myChart.data.labels = this.props.data.map(d => d.createdAt);
    // this.myChart.data.datasets[0].data = this.props.data.map(d => d.x);
    // this.myChart.data.datasets[1].data = this.props.data.map(d => d.y);
    this.myChart.data.datasets[0].data = this.props.data.map(d => d.y);
    this.myChart.update();
  }

  componentDidMount() {
    let datos = []

    // datos.push({
    //   label: 'Rotacion en X',
    //   data: this.props.data.map(d => parseFloat(d.valor)),
    //   fill: '',
    //   backgroundColor: this.props.colorX,
    //   pointRadius: 2,
    //   borderColor: this.props.colorX,
    //   borderWidth: 3,
    //   lineTension: 0
    // })

    datos.push({
      label: 'Oleaje',
      data: this.props.data.map(d => parseFloat(d.y)),
      fill: '',
      backgroundColor: this.props.colorY,
      pointRadius: 2,
      borderColor: this.props.colorY,
      borderWidth: 3,
      lineTension: 0
    })

    this.myChart = new Chart(this.chartRef.current, {
      type: 'line',
      options: {
        tooltips: {
          enabled: true,
          mode: 'single',
          callbacks: {
            label: (item) => {
              return `${item.yLabel}`;
            }
          }
        },
        scales: {
          yAxes: [{
            stacked: false,
            ticks: {
              // Include a dollar sign in the ticks
              callback: function (value, index, values) {
                return value;
              }
            }
          }]
        },
      },
      data: {
        labels: this.props.data.map(d => `${d.createdAt}`),
        datasets: datos
      }
    });

  }

  render() {
    return <canvas ref={this.chartRef} />;
  }
}

export default LineChart