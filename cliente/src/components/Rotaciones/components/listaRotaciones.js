import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment'


import History from '../../../history.js';
import LineChart from './lineChart'


class ListaRotaciones extends Component {


  constructor(props) {
    super(props)

    this.state = { rotaciones: [], center: [], zoom: 14 }
  }

  componentWillReceiveProps(props) {
    if (props.rotaciones) {
      let rotaciones = props.rotaciones
      rotaciones = rotaciones.map(rotacion => {
        rotacion.createdAt = moment(rotacion.createdAt).format('YYYY/MM/DD HH:mm:ss')
        return rotacion
      })
      this.setState({ rotaciones: rotaciones })
    }

  }



  render() {
    let rotaciones = this.state.rotaciones
    let nombre = 'Medicion de Oleaje'

    if (rotaciones.length > 0){
      rotaciones = rotaciones.slice(0, 99)
    }

    if (rotaciones.length > 0)
      return (
        <div className="col-sm-12">

          <div className="card card-primary">
            <div className="card-header">
              <h3 className="card-title">{nombre}</h3>
              <div className="card-tools">
                <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus" />
                </button>
                <button type="button" className="btn btn-tool" data-card-widget="remove"><i className="fas fa-times" /></button>
              </div>
            </div>
            <div className="card-body">
              {<LineChart data={rotaciones} title={nombre} colorX="blue" colorY="green" />}
            </div>
          </div>




          <div className="card card-primary">
            <div className="card-header">
              <h3 className="card-title">Historial de mediciones</h3>
            </div>
            {/* /.card-header */}
            <div className="card-body table-responsive p-0">
              <table className="table table-hover text-nowrap">
                <thead>
                  <tr>
                    <th>Hora</th>
                    <th>Valor</th>
                  </tr>
                </thead>
                <tbody>

                  {this.state.rotaciones.map(rotacion => {
                    return (
                      <tr key={rotacion._id} className={rotacion.alerta ? "table-danger" : "table-success"}>
                        <td>{rotacion.createdAt}</td>
                        <td>{rotacion.y}</td>
                      </tr>
                    )

                  })}
                </tbody>
              </table>
            </div>
            {/* /.card-body */}
          </div>


          <hr />


        </div >
      )
    return (
      <div>
        <p>No hay mediciones</p>
        <button className="btn btn-default" onClick={History.goBack}>Volver</button>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {}
};



export default connect(mapStateToProps, {})(ListaRotaciones);
