import React, { Component } from "react";

class MostrarUsuario extends Component {

  render() {
    let user = this.props.user
    let divisiones = []
    let instituciones = []
    if (user.roles) {
      divisiones = user.roles.map(r => r.divisiones_organizativa ? r.divisiones_organizativa.nombreLargo : '')
      instituciones = user.roles.map(r => r.institucione ? r.institucione.nombreLargo : '')
    }


    if (!this.props.mostrar)
      return null
    return (
      <div className="row">
        <div className="col-md-12">


          <div className="card bg-dark">
            <div className="card-header">
              <h3 className="card-title">Datos del usuario {user.username || '---'}</h3>
              <div className="card-tools">
                <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus"></i></button>
                <button type="button" className="btn btn-tool" onClick={this.props.salir.bind(null)}><i className="fas fa-times" /></button>

              </div>
            </div>
            {/* /.card-header */}
            <div className="card-body">

              <div className="row">
                <div className="col-md-4">
                  <div className="row">
                    <dt className="col-md-6">Username</dt>
                    <dd className="col-md-6">{user.username || '---'}</dd>


                    <dt className="col-md-6">Roles</dt>
                    <dd className="col-md-6">{user.roles.map(rol => <div className="btn btn-xs btn-info" style={{ margin: 5 }}>{rol.nombreLargo || '---'}</div>) || '---'}</dd>


                    <dt className="col-md-6">Nombre</dt>
                    <dd className="col-md-6">{user.nombre || '---'}</dd>
                    <dt className="col-md-6">apellido</dt>
                    <dd className="col-md-6">{user.apellido || '---'}</dd>
                    <dt className="col-md-6">Documento</dt>
                    <dd className="col-md-6">
                      <div className="btn btn-xs btn-info" style={{ marginLeft: 10, marginRight: 10 }}>
                        {/* <i className="nav-icon fas fa-info" />  */}
                        {user.identificacionTipo || '---'}
                      </div>
                      {user.identificacion || '---'}
                    </dd>
                    <dt className="col-md-6">genero</dt>
                    <dd className="col-md-6">{user.genero || '---'}</dd>
                    <dt className="col-md-6">nacionalidad</dt>
                    <dd className="col-md-6">{user.nacionalidad || '---'}</dd>
                    <dt className="col-md-6">fechaNacimiento</dt>
                    <dd className="col-md-6">{user.fechaNacimiento || '---'}</dd>
                    <dt className="col-md-6">Edad</dt>
                    <dd className="col-md-6">{user.edad || '---'}</dd>
                    <dt className="col-md-6">direccion1</dt>
                    <dd className="col-md-6">{user.direccion1 || '---'}</dd>
                    <dt className="col-md-6">dereccion2</dt>
                    <dd className="col-md-6">{user.dereccion2 || '---'}</dd>

                  </div>
                </div>
                <div className="col-md-4">
                  <div className="row">

                    <dt className="col-md-6">localidad</dt>
                    <dd className="col-md-6">{user.localidad || '---'}</dd>
                    <dt className="col-md-6">provincia</dt>
                    <dd className="col-md-6">{user.provincia || '---'}</dd>
                    <dt className="col-md-6">pais</dt>
                    <dd className="col-md-6">{user.pais || '---'}</dd>
                    <dt className="col-md-6">email</dt>
                    <dd className="col-md-6">{user.email || '---'}</dd>
                    <dt className="col-md-6">habilitado</dt>
                    <dd className="col-md-6">{user.habilitado ? <div className="btn btn-xs btn-success" style={{ padding: 5 }}>si</div> : <div className="btn btn-xs btn-danger" style={{ padding: 5 }}>no</div> || '---'}</dd>
                    <dt className="col-md-6">createdAt</dt>
                    <dd className="col-md-6">{user.createdAt || '---'}</dd>
                    <dt className="col-md-6">updatedAt</dt>
                    <dd className="col-md-6">{user.updatedAt || '---'}</dd>

                    <dt className="col-md-6">creador</dt>
                    <dd className="col-md-6">{user.creador ? user.creador.username : '' || '---'}</dd>
                    <dt className="col-md-6">tels</dt>
                    <dd className="col-md-6">{user.tels.map(tel => <div className="btn btn-xs btn-info" style={{ margin: 5 }}>{tel.tel} ({tel.tipo})</div>) || '---'}</dd>



                  </div>


                </div>
                <div className="col-md-4">
                  <dt className="col-md-6">Instituciones</dt>
                  <dd className="col-md-6">{instituciones.map(inst => inst ? <div className="btn btn-xs btn-info" style={{ margin: 5 }}>{inst}</div> : '')}</dd>
                  <dt className="col-md-6">Div. Org</dt>
                  <dd className="col-md-6">{divisiones.map(div => div ? <div className="btn btn-xs btn-info" style={{ margin: 5 }}>{div}</div> : '')}</dd>

                  {user.campos.map(campo => {
                    return <div className="row">
                      <dt className="col-md-6">{campo.lista_campo.nombre || '---'}</dt>
                      <dd className="col-md-6">{campo.valor || '---'}</dd>
                    </div>
                  })}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default MostrarUsuario