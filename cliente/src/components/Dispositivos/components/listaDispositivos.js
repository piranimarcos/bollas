import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Circle, Map, TileLayer, Marker, Popup } from 'react-leaflet'

import "./../../../css/app.css";
import { NavLink } from 'react-router-dom';

class ListaDispositivos extends Component {

  constructor(props) {
    super(props)


    this.state = {
      // lat: 51.505,
      // lng: -0.09,

      lat: -33.226246,
      lng: -60.320222,

      zoom: 14, dispositivo: []
    }

  }

  componentWillReceiveProps(props) {
    if (props.dispositivo) {
      this.setState({ dispositivo: props.dispositivo })
    }
  }


  render() {

    const position = [this.state.lat, this.state.lng]

    const center = [-33.25369, -60.304878]


    return (
      <Map center={position} zoom={this.state.zoom}>
        <TileLayer
          attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        {/* <Marker position={position}>
          <Popup>
            Dispositivo #1. <br /> Bolla 1.
          </Popup>
        </Marker>
        <Circle center={position} fillColor="green" color="green" radius={50} >
          <Popup>Radio límite</Popup>
        </Circle> */}
        {this.state.dispositivo.map((dispositivo, i) => {
          let pos = [dispositivo.posicion.lat, dispositivo.posicion.lng]
          return (
            <div>
              <Marker position={pos}>

                <Popup className="text-center">
                  <h4 style={{textAlign: "center"}}>{dispositivo.nombre}</h4>
                  <br />
                  <NavLink className="btn btn-app" exact to={`/dashboard/dispositivos/rotacion/${dispositivo._id}`}>
                    <i className="fas fa-water" /> Oleaje
                  </NavLink>
                  <NavLink className="btn btn-app" exact to={`/dashboard/dispositivos/${dispositivo._id}`}>
                    <i className="fas fa-map-marked-alt" /> Posicion
</NavLink>

                </Popup>
              </Marker>
              <Circle center={pos} fillColor="green" color="green" radius={50} >
                <Popup>Radio límite</Popup>
              </Circle>
            </div>
          )
        })}
      </Map>
    )
  }
}

const mapStateToProps = (state) => {
  return {}
};



export default connect(mapStateToProps, {})(ListaDispositivos);
