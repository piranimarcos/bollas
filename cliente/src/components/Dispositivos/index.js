import React, { Component } from 'react';
import { connect } from 'react-redux'


import {ToastContainer ,toast} from 'react-toastify'

import 'react-toastify/dist/ReactToastify.css';
import ListaDispositivos from './components/listaDispositivos';

import * as dispositivos from '../../actions/dispositivos';

class Dispositivos extends Component {

  renderError() {
    if (this.props.error)
      toast.error(this.props.error, { position: "top-right", autoClose: 5000, hideProgressBar: false, closeOnClick: true, pauseOnHover: true, draggable: true })


    if (this.props.message)
      toast.success(this.props.message, { position: "top-right", autoClose: 5000, hideProgressBar: false, closeOnClick: true, pauseOnHover: true, draggable: true });
    
  }

  componentWillMount(){
    this.props.fetchDispositivos();
  }

  renderLoading() {
    return <div className="overlay"><i className="fas fa-2x fa-sync-alt fa-spin" /></div>
  }


  render() {

    return (
      <div>

        {this.renderError()}

        <ListaDispositivos dispositivo={this.props.dispositivos}/>
       
        <ToastContainer />
      </div>
    );

  }
}

const mapStateToProps = (state) => {
  return {
    dispositivos: state.dispositivo.dispositivos,
    error: state.dispositivo.error,
    message: state.dispositivo.message
  }
};



export default connect(mapStateToProps, dispositivos)(Dispositivos);
