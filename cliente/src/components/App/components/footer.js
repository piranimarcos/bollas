
import React from 'react';

const Footer = () =>
  <footer className="main-footer">
    <strong>Copyright © 2020</strong> Hecho con <i className="nav-icon fas fa-heart" />
    <div className="float-right d-none d-sm-inline-block">
      <b>Version</b> 1.0
    </div>
  </footer>

export default Footer;