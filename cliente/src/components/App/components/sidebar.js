import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';

import logo from '../../../assets/img/logo.png'
import usuario from '../../../assets/img/user2-160x160.jpg'

class Sidebar extends PureComponent {
  constructor(props){
    super()

    this.state = {user: {}}
    
  }
  
  render() {
    let user = this.state.user || ''
    return (
      <aside className="main-sidebar sidebar-dark-primary elevation-4">
        {/* Brand Logo */}
        <div className="brand-link">
          <img src={logo} alt="AdminLTE Logo" className="brand-image img-circle elevation-3" style={{ opacity: '.8' }} />
          <span className="brand-text font-weight-light">Panel de Control</span>
        </div>
        {/* Sidebar */}
        <div className="sidebar">

          <div className="user-panel mt-3 pb-3 mb-3 d-flex">
            <div className="image">
              <img src={usuario} className="img-circle elevation-2" alt="User" />
            </div>
            <div className="info">
              <a className="d-block">Admin</a>
            </div>
          </div>

          {/* Sidebar Menu */}
          <nav className="mt-2">
            <ul className="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
              {/* Add icons to the links using the .nav-icon class with font-awesome or any other icon font library */}
              <li className="nav-item has-treeview menu-open">
                <NavLink exact to="/dashboard" className="nav-link" activeClassName="active">
                  <i className="nav-icon fas fa-tachometer-alt" />
                  <p>
                    Dashboard
                  </p>
                </NavLink>
              </li>

              {/* <li className="nav-item has-treeview menu-open">
                <NavLink to="/dashboard/usuarios" className="nav-link" activeClassName="active">
                  <i className="nav-icon fas fa-user" />
                  <p>
                    Usuario
                  </p>
                </NavLink>
              </li> */}

              <li className="nav-item has-treeview menu-open">
                <NavLink to="/dashboard/sensores" className="nav-link" activeClassName="active">
                  <i className="nav-icon fas fa-building" />
                  <p>
                    Sensores
                  </p>
                </NavLink>
              </li>

              <li className="nav-item has-treeview menu-open">
                <NavLink to="/dashboard/dispositivos" className="nav-link" activeClassName="active">
                  <i className="nav-icon fas fa-notes-medical" />
                  <p>
                    Dispositivos
                  </p>
                </NavLink>
              </li>

              <li className="nav-item has-treeview menu-open">
                <NavLink to="/dashboard/galeria" className="nav-link" activeClassName="active">
                  <i className="nav-icon fas fa-camera" />
                  <p>
                    Galeria
                  </p>
                </NavLink>
              </li>

            </ul>
          </nav>
          {/* /.sidebar-menu */}
        </div>
        {/* /.sidebar */}
      </aside>

    );
  }
}

const mapStateToProps = (state) => {
  return { authenticated: state.auth.authenticated }
};

export default connect(mapStateToProps)(Sidebar);

