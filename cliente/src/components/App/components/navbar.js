import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { signoutUser } from '../../../actions'

class Navbar extends PureComponent {

  renderLinks() {
    if (this.props.authenticated) {
      return (
        <li className="nav-item">
          <div className="nav-link" onClick={() => this.props.signOut()}>Salir</div>
        </li>
      );
    }
  }

  render() {
    return (
      <nav className="main-header navbar navbar-expand navbar-white navbar-light">


        <ul className="navbar-nav">
          <li className="nav-item">
            <div className="nav-link" data-widget="pushmenu" role="button"><i className="fas fa-bars" /></div>
          </li>
        </ul>

        <ul className="navbar-nav ml-auto">

          {this.renderLinks()}
        </ul>
      </nav>
    );
  }
}

const mapStateToProps = (state) => {
  return { authenticated: state.auth.authenticated }
};


export default connect(mapStateToProps, { signOut: signoutUser })(Navbar);
