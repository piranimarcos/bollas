import React, { Component } from 'react';
import Navbar from './components/navbar'
import Sidebar from './components/sidebar'
import Footer from './components/footer'


import { ToastContainer, toast } from 'react-toastify'
import socketIOClient from "socket.io-client";
import { SOCKET } from '../../routes/config'

class App extends Component {

  componentDidMount() {
    const socket = socketIOClient(SOCKET);
    // socket.on("nuevaMedicion", (data, nombre) => {
    //   data = JSON.parse(data)
    //   console.log(data)
    //   toast.error('Nueva medicion en "'+nombre+'"', { position: "top-right", autoClose: 5000, hideProgressBar: false, closeOnClick: true, pauseOnHover: true, draggable: true })
    // });
    // socket.on("nuevaRotacion", data => {
    //   data = JSON.parse(data)
    //   console.log(data)
    //   toast.error('Nueva rotacion en "'+data.nombreSocket+'"', { position: "top-right", autoClose: 5000, hideProgressBar: false, closeOnClick: true, pauseOnHover: true, draggable: true })
    // });

    socket.on("nuevaAlertaMedicion", (data, nombre) => {
      data = JSON.parse(data)
      console.log(data)
      toast.error('Nueva alerta en sensor '+nombre, { position: "top-right", autoClose: 5000, hideProgressBar: false, closeOnClick: true, pauseOnHover: true, draggable: true })
    });

    socket.on("nuevaAlertaPosicion", (data, nombre) => {
      data = JSON.parse(data)
      console.log(data)
      toast.error('Posicion de '+nombre+' fuera de limite', { position: "top-right", autoClose: 5000, hideProgressBar: false, closeOnClick: true, pauseOnHover: true, draggable: true })
    });
  }

  render() {

    return (
      <div className="hold-transition sidebar-mini" id="body">
        <div className="wrapper">

          <Navbar />

          <Sidebar />

          {/* Content Wrapper. Contains page content */}
          <div className="content-wrapper">
            {/* Content Header (Page header) */}
            <div className="content-header">
              <div className="container-fluid">

                {this.props.children}

              </div>
            </div>
          </div>

          <Footer />

        </div>

        <ToastContainer />

      </div>

    );
  }
}

export default App;
