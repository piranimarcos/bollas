import React, { Component } from 'react';
import { connect } from 'react-redux'


import { ToastContainer, toast } from 'react-toastify'
import Card from './components/Card'

import 'react-toastify/dist/ReactToastify.css';

import { fetchDispositivos } from '../../actions/dispositivos';
import { fetchUltimaMedicion } from '../../actions/mediciones';

class Dashboard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
    };
  }


  renderError() {
    if (this.props.error)
      toast.error(this.props.error, { position: "top-right", autoClose: 5000, hideProgressBar: false, closeOnClick: true, pauseOnHover: true, draggable: true })


    if (this.props.message)
      toast.success(this.props.message, { position: "top-right", autoClose: 5000, hideProgressBar: false, closeOnClick: true, pauseOnHover: true, draggable: true });

  }

  renderLoading() {
    return <div className="overlay"><i className="fas fa-2x fa-sync-alt fa-spin" /></div>
  }

  componentWillMount() {
    this.props.dispatch(fetchDispositivos())
  }


  render() {
    let dispositivos = this.props.dispositivos

    if (dispositivos && dispositivos.length > 0)

      return (
        <div>

          {this.renderError()}

          {this.props.dispositivos.map(disp => {
            return <Card dispositivo={disp} id={disp._id}/>
          })}


          <ToastContainer />

        </div>
      );

    return (this.renderLoading())
  }
}

const mapStateToProps = (state) => {
  return {
    dispositivos: state.dispositivo.dispositivos,
    error: state.sensor.error,
    message: state.sensor.message
  }
};

const mapDispatchToProps = () => {
  return {
    fetchDispositivos
  }
}



export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
