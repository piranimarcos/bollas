import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';


import socketIOClient from "socket.io-client";
import { SOCKET } from '../../../routes/config'

import { API_V1 } from '../../../routes/config'

class Card extends Component {

  constructor(props) {
    super(props)

    this.state = { data: [] }
  }

  componentDidMount() {
    let id = this.props.id
    if (id) {
      fetch(API_V1 + '/ultima-medicion?id=' + id)
        .then(response => response.json())
        .then(data => {
          this.setState({ data: data.mediciones }, () => console.log(this.state))
        });
    }
    const socket = socketIOClient(SOCKET);
    socket.on("nuevaMedicion", (datos, nombre) => {
      let medicion = JSON.parse(datos)
      console.log('nueva Medicion -->' + JSON.stringify(datos))

      let data = this.state.data

      data.map((d, i) => {
        console.log(d)
        if (d != null && d.idSensor._id == medicion.idSensor) {
          medicion.idSensor = d.idSensor
          data[i] = medicion
        }
      })
      this.setState({ data: data }, () => console.log(this.state))

    });
  }

  valoresLegibles(medicion) {
    if (medicion.idSensor.tipo == 'digital') {
      if (medicion.alerta) {
        medicion.valor = 'Fallando'
      } else {
        medicion.valor = 'OK'
      }
    }
  }


  render() {
    let data = this.state.data

    if (data && data.length > 0)
      return (
        <div className="row">

          <div className="col-md-12">

            <div className="card card-lightblue">
              <div className="card-header">
                <h3 className="card-title">{this.props.dispositivo.nombre}</h3>
                <div className="card-tools">
                  <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus" />
                  </button>
                </div>
                {/* /.card-tools */}
              </div>
              {/* /.card-header */}
              <div className="card-body">


                <div className="row">


                  {data.map((medicion, e) => {
                    if (medicion != null) {
                      // this.valoresLegibles(medicion)

                      if (medicion.idSensor.nombreRef != 'rotacionX' && medicion.idSensor.nombreRef != 'rotacionY')
                        return (
                          <div className="col-sm-4">
                            <div className={medicion.alerta == true ? "small-box bg-danger" : "small-box bg-success"}>
                              <div className="inner">
                                <h3>{medicion.valor}<sup style={{ fontSize: 20 }}>{medicion.idSensor.unidad}</sup></h3>
                                <p>{medicion.idSensor.nombre}</p>
                              </div>
                              <div className="icon">
                                <i className="fas fa-cog" />
                              </div>
                              <NavLink className="small-box-footer" exact to={`/dashboard/sensores/${medicion.idSensor._id}`}>
                                Saber más <i className="fas fa-arrow-circle-right" />
                              </NavLink>
                            </div>
                          </div>
                        )
                    }
                  })}
                </div>

              </div>
            </div>
            {/* /.card-body */}
          </div>

        </div>
      )

    return (<div></div>)
  }
}

const mapStateToProps = (state) => {
  return {}
};



export default connect(mapStateToProps, {})(Card);
