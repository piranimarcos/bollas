import React, { PureComponent } from 'react';
import { Field, reduxForm } from 'redux-form';
import * as actions from '../../actions';
import { connect } from 'react-redux';

class Signin extends PureComponent {

    handleFormSubmit({ username, password }) {
        this.props.signinUser({ username, password })
    }

    renderField = ({ input, label, type, icon, meta: { touched, error } }) => {
        return (
            <div>

                <label>{label}</label>
                <div className="input-group mb-3">
                    <input type={type} className="form-control" placeholder={label} {...input} />
                    <div className="input-group-append">
                        <div className="input-group-text">
                            <span className={icon} />
                        </div>
                    </div>
                </div>
                {touched && error && <span className="text-danger">{error}</span>}
            </div>
        )
    };

    renderError() {
        if (this.props.errorMessage) {
            return (
                <div className="alert alert-danger">
                    <string>{this.props.errorMessage}</string>
                </div>
            );
        }
    }

    render() {
        const { handleSubmit } = this.props;
        console.log(handleSubmit);


        return (
            <div className="hold-transition login-page">

                <div className="login-box">
                    <div className="login-logo">
                        <b>Boyas</b> 1.0
                    </div>
                    {/* /.login-logo */}
                    <div className="card">
                        <div className="card-body login-card-body">

                            <p className="login-box-msg">Ingrese los datos para iniciar sesion</p>
                            {this.renderError()}

                            <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
                                <fieldset className="form-group">
                                    <Field
                                        name="username"
                                        label="Usuario"
                                        component={this.renderField}
                                        type="text"
                                        icon="fa fa-envelope" />
                                </fieldset>
                                <fieldset className="form-group">
                                    <Field
                                        name="password"
                                        label="Contraseña"
                                        component={this.renderField}
                                        type="password"
                                        icon="fa fa-lock" />
                                </fieldset>

                                <div className="row">
                                    <div className="col-4">
                                        <button type="submit" className="btn btn-primary btn-block">Ingresar</button>
                                    </div>
                                </div>
                            </form>

                        </div>
                        {/* /.login-card-body */}
                    </div>
                </div>
            </div>





        );
    }
}



const validate = values => {
    console.log(values);
    const errors = {};

    if (!values.username) 
        errors.username = 'Ingrese usuario';
    if (!values.password) 
        errors.password = 'Ingrese clave';

    return errors;
};

const mapStateToProps = (state) => {
    return { errorMessage: state.auth.error || state.auth.expired }
};

export default reduxForm({
    form: 'signin',
    validate
})(connect(mapStateToProps, actions)(Signin));
