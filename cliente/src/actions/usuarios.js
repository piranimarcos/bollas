import axios from 'axios';
// import History from '../history.js';
import {
    GET_USER,
    ERROR_USER,
    CREATE_USER,
    UPDATE_USER,
    CLEAN_MSJ,
    DELETE_USER
} from './types';

import { API_V1 } from '../routes/config'


export const fetchUsuarios = () => {
    return (dispatch) => {
        axios.get(`${API_V1}/users`, {
            headers: { "x-access-token": localStorage.getItem('token') }
        })
            .then(response => {
                dispatch({
                    type: GET_USER,
                    payload: response.data
                });
            }).catch(err => {

                dispatch(userError(err.message))
            })
    };
};

export const crearUsuario = (user) => {

    console.log(user);
    
    return (dispatch) => {
        dispatch(clearMsj())
        axios.post(`${API_V1}/users`, user, {
            headers: { "x-access-token": localStorage.getItem('token') }
        }).then(response => {
            dispatch({ type: CREATE_USER, payload: response.data.message });
            dispatch(fetchUsuarios())
        }).catch(err => {

            dispatch(userError(err.response.data.message))
        })
    };
};


export const moficarUsuario = (user) => {

    console.log(user);
    
    return (dispatch) => {
        dispatch(clearMsj())
        axios.put(`${API_V1}/users/${user.idUser}`, user, {
            headers: { "x-access-token": localStorage.getItem('token') }
        }).then(response => {
            dispatch({ type: UPDATE_USER, payload: response.data.message });
            dispatch(fetchUsuarios())
        }).catch(err => {

            dispatch(userError(err.response.data.message))
        })
    };
};

export const borrarUsuario = (id) => {
    
    return (dispatch) => {
        dispatch(clearMsj())
        axios.delete(`${API_V1}/users/${id}`, {
            headers: { "x-access-token": localStorage.getItem('token') }
        }).then(response => {
            console.log(response);
            
            dispatch({ type: DELETE_USER, payload: response.data.message });
            dispatch(fetchUsuarios())
        }).catch(err => {
            dispatch(userError(err.response.data.message))
        })
    };
};

export const userError = (error) => {
    return {
        type: ERROR_USER,
        payload: error
    };
};


export const clearMsj = () => {
    return {
        type: CLEAN_MSJ
    };
};
