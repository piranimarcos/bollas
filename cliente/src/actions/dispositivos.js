import axios from 'axios';
// import History from '../history.js';
import {
    GET_DISPOSITIVO,
    ERROR_DISPOSITIVO,
    CREATE_DISPOSITIVO,
    UPDATE_DISPOSITIVO,
    CLEAN_MSJ_DISPOSITIVO,
    DELETE_DISPOSITIVO
} from './types';

import { API_V1 } from '../routes/config'


export const fetchDispositivos = () => {
    return (dispatch) => {
        axios.get(`${API_V1}/dispositivos`, {
            headers: { "x-access-token": localStorage.getItem('token') }
        })
            .then(response => {
                dispatch({
                    type: GET_DISPOSITIVO,
                    payload: response.data
                });
            })
            .catch(err => {
                dispatch(dispositivoError(err.message))
            })
    };
};

export const getDispositivo = (id) => {
    return (dispatch) => {
        axios.get(`${API_V1}/dispositivos?conditions={ "_id": "${id}" }`, {
            headers: { "x-access-token": localStorage.getItem('token') }
        })
            .then(response => {
                dispatch({
                    type: GET_DISPOSITIVO,
                    payload: response.data
                });
            })
            .catch(err => {
                dispatch(dispositivoError(err.message))
            })
    };
};

export const crearInstitucion = (dispositivo) => {

    console.log(dispositivo);
    
    return (dispatch) => {
        dispatch(clearMsj())
        axios.post(`${API_V1}/dispositivos`, dispositivo, {
            headers: { "x-access-token": localStorage.getItem('token') }
        }).then(response => {
            dispatch({ type: CREATE_DISPOSITIVO, payload: response.data.message });
            dispatch(fetchDispositivos())
        }).catch(err => {

            dispatch(dispositivoError(err.response.data.message))
        })
    };
};

export const modificarInstitucion = (dispositivo) => {
    return (dispatch) => {
        axios.put(`${API_V1}/dispositivos`, {
            dispositivo,
            headers: { 'x-access-token': localStorage.getItem('token') }
        })
            .then(response => {
                console.log(response);

                dispatch({ type: UPDATE_DISPOSITIVO, payload: response.data.message });
            })
            .catch(err => {
                dispatch(dispositivoError(err.response.data.message))
            })
    };
};

export const borrarInstitucion = (id) => {
    
    return (dispatch) => {
        dispatch(clearMsj())
        axios.delete(`${API_V1}/dispositivos/${id}`, {
            headers: { "x-access-token": localStorage.getItem('token') }
        }).then(response => {
            console.log(response);
            
            dispatch({ type: DELETE_DISPOSITIVO, payload: response.data.message });
            dispatch(fetchDispositivos())
        }).catch(err => {
            dispatch(dispositivoError(err.response.data.message))
        })
    };
};

export const dispositivoError = (error) => {
    return {
        type: ERROR_DISPOSITIVO,
        payload: error
    };
};


export const clearMsj = () => {
    return {
        type: CLEAN_MSJ_DISPOSITIVO
    };
};
