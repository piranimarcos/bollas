import axios from 'axios';
// import History from '../history.js';
import {
    GET_MEDICION,
    ERROR_MEDICION,
    CREATE_MEDICION,
    UPDATE_MEDICION,
    CLEAN_MSJ_MEDICION,
    DELETE_MEDICION
} from './types';

import { API_V1 } from '../routes/config'


export const fetchMedicions = (sensor) => {
    return (dispatch) => {
        // axios.get(`${API_V1}/medicions?populate=idSensor&conditions={ "idSensor": "${sensor}" }`, {
        axios.get(`${API_V1}/medicions?conditions={ "idSensor": "${sensor}" }&sort=-createdAt`, {
            headers: { "x-access-token": localStorage.getItem('token') }
        })
            .then(response => {
                dispatch({
                    type: GET_MEDICION,
                    payload: response.data
                });
            })
            .catch(err => {
                dispatch(medicionError(err.message))
            })
    };
};



export const fetchUltimaMedicion = (dispositivo) => {
    return (dispatch) => {
        axios.get(`${API_V1}/ultima-medicion?id=${dispositivo} }`, {
            headers: { "x-access-token": localStorage.getItem('token') }
        })
            .then(response => {
                dispatch({
                    type: GET_MEDICION,
                    payload: response.data
                });
            })
            .catch(err => {
                dispatch(medicionError(err.message))
            })
    };
};



export const getRotacion = (rotacionX, rotacionY) => {
    return (dispatch) => {
        axios.get(`${API_V1}/rotacion?rotacionX=${rotacionX}&rotacionY=${rotacionY} }`, {
            headers: { "x-access-token": localStorage.getItem('token') }
        })
            .then(response => {
                dispatch({
                    type: GET_MEDICION,
                    payload: response.data
                });
            })
            .catch(err => {
                dispatch(medicionError(err.message))
            })
    };
};


export const medicionError = (error) => {
    return {
        type: ERROR_MEDICION,
        payload: error
    };
};


export const clearMsj = () => {
    return {
        type: CLEAN_MSJ_MEDICION
    };
};
