import axios from 'axios';
// import History from '../history.js';
import {
    GET_INSTITUCION,
    ERROR_INSTITUCION,
    CREATE_INSTITUCION,
    UPDATE_INSTITUCION,
    CLEAN_MSJ_INSTITUCION,
    DELETE_INSTITUCION
} from './types';

import { API_V1 } from '../routes/config'


export const fetchInstituciones = () => {
    return (dispatch) => {
        axios.get(`${API_V1}/institucions`, {
            headers: { "x-access-token": localStorage.getItem('token') }
        })
            .then(response => {
                dispatch({
                    type: GET_INSTITUCION,
                    payload: response.data
                });
            })
            .catch(err => {
                dispatch(institucionError(err.message))
            })
    };
};

export const crearInstitucion = (institucion) => {

    console.log(institucion);
    
    return (dispatch) => {
        dispatch(clearMsj())
        axios.post(`${API_V1}/institucions`, institucion, {
            headers: { "x-access-token": localStorage.getItem('token') }
        }).then(response => {
            dispatch({ type: CREATE_INSTITUCION, payload: response.data.message });
            dispatch(fetchInstituciones())
        }).catch(err => {

            dispatch(institucionError(err.response.data.message))
        })
    };
};

export const modificarInstitucion = (institucion) => {
    return (dispatch) => {
        axios.put(`${API_V1}/institucions`, {
            institucion,
            headers: { 'x-access-token': localStorage.getItem('token') }
        })
            .then(response => {
                console.log(response);

                dispatch({ type: UPDATE_INSTITUCION, payload: response.data.message });
            })
            .catch(err => {
                dispatch(institucionError(err.response.data.message))
            })
    };
};

export const borrarInstitucion = (id) => {
    
    return (dispatch) => {
        dispatch(clearMsj())
        axios.delete(`${API_V1}/institucions/${id}`, {
            headers: { "x-access-token": localStorage.getItem('token') }
        }).then(response => {
            console.log(response);
            
            dispatch({ type: DELETE_INSTITUCION, payload: response.data.message });
            dispatch(fetchInstituciones())
        }).catch(err => {
            dispatch(institucionError(err.response.data.message))
        })
    };
};

export const institucionError = (error) => {
    return {
        type: ERROR_INSTITUCION,
        payload: error
    };
};


export const clearMsj = () => {
    return {
        type: CLEAN_MSJ_INSTITUCION
    };
};
