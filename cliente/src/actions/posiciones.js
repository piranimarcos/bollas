import axios from 'axios';
// import History from '../history.js';
import {
    GET_POSICION,
    ERROR_POSICION,
    CLEAN_MSJ_POSICION
} from './types';

import { API_V1 } from '../routes/config'


export const fetchPosicions = (dispositivo) => {
    return (dispatch) => {
        axios.get(`${API_V1}/posicions?conditions={ "idDispositivo": "${dispositivo}" }&sort=-createdAt&limit=100`, {
            headers: { "x-access-token": localStorage.getItem('token') }
        })
            .then(response => {
                dispatch({
                    type: GET_POSICION,
                    payload: response.data
                });
            })
            .catch(err => {
                dispatch(posicionError(err.message))
            })
    };
};


export const posicionError = (error) => {
    return {
        type: ERROR_POSICION,
        payload: error
    };
};


export const clearMsj = () => {
    return {
        type: CLEAN_MSJ_POSICION
    };
};
