import axios from 'axios';
// import History from '../history.js';
import {
    GET_ROLE,
    ERROR_ROLE,
    CREATE_ROLE,
    UPDATE_ROLE,
    CLEAN_MSJ_ROLE,
    DELETE_ROLE
} from './types';

import { API_V1 } from '../routes/config'


export const fetchRoles = () => {
    return (dispatch) => {
        axios.get(`${API_V1}/roles`, {
            headers: { "x-access-token": localStorage.getItem('token') }
        })
            .then(response => {
                dispatch({
                    type: GET_ROLE,
                    payload: response.data
                });
            })
            .catch(err => {
                dispatch(roleError(err.message))
            })
    };
};

export const crearRole = (role) => {

    console.log(role);
    
    return (dispatch) => {
        dispatch(clearMsj())
        axios.post(`${API_V1}/roles`, role, {
            headers: { "x-access-token": localStorage.getItem('token') }
        }).then(response => {
            dispatch({ type: CREATE_ROLE, payload: response.data.message });
            dispatch(fetchRoles())
        }).catch(err => {

            dispatch(roleError(err.response.data.message))
        })
    };
};

export const modificarRole = (role) => {
    return (dispatch) => {
        axios.put(`${API_V1}/roles`, {
            role,
            headers: { 'x-access-token': localStorage.getItem('token') }
        })
            .then(response => {
                console.log(response);

                dispatch({ type: UPDATE_ROLE, payload: response.data.message });
            })
            .catch(err => {
                dispatch(roleError(err.response.data.message))
            })
    };
};

export const borrarRole = (id) => {
    
    return (dispatch) => {
        dispatch(clearMsj())
        axios.delete(`${API_V1}/roles/${id}`, {
            headers: { "x-access-token": localStorage.getItem('token') }
        }).then(response => {
            console.log(response);
            
            dispatch({ type: DELETE_ROLE, payload: response.data.message });
            dispatch(fetchRoles())
        }).catch(err => {
            dispatch(roleError(err.response.data.message))
        })
    };
};

export const roleError = (error) => {
    return {
        type: ERROR_ROLE,
        payload: error
    };
};


export const clearMsj = () => {
    return {
        type: CLEAN_MSJ_ROLE
    };
};
