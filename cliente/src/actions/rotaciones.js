import axios from 'axios';
// import History from '../history.js';
import {
    GET_ROTACION,
    ERROR_ROTACION,
    CLEAN_MSJ_ROTACION
} from './types';

import { API_V1 } from '../routes/config'


export const fetchRotacions = (dispositivo) => {
    return (dispatch) => {
        axios.get(`${API_V1}/rotacions?conditions={ "idDispositivo": "${dispositivo}" }&sort=-createdAt`, {
            headers: { "x-access-token": localStorage.getItem('token') }
        })
            .then(response => {
                dispatch({
                    type: GET_ROTACION,
                    payload: response.data
                });
            })
            .catch(err => {
                dispatch(rotacionError(err.message))
            })
    };
};


export const rotacionError = (error) => {
    return {
        type: ERROR_ROTACION,
        payload: error
    };
};


export const clearMsj = () => {
    return {
        type: CLEAN_MSJ_ROTACION
    };
};
