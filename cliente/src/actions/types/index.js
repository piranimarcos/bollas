export const AUTH_USER = 'auth_user';
export const UNAUTH_USER = 'unauth_user';
export const AUTH_ERROR = 'auth_error';
export const AUTH_EXPIRED = 'auth_expired';
export const AUTH_NOT_EXPIRED = 'auth_not_expired';
export const FETCH_FEATURE = 'fetch_feature';


export const GET_USER = 'get_user';
export const ERROR_USER = 'error_user';
export const CREATE_USER = 'create_user';
export const UPDATE_USER = 'update_user';
export const DELETE_USER = 'delete_user';
export const CLEAN_MSJ = 'clean_msj';


export const GET_ROLE = 'get_role';
export const ERROR_ROLE = 'error_role';
export const CREATE_ROLE = 'create_role';
export const UPDATE_ROLE = 'update_role';
export const DELETE_ROLE = 'delete_role';
export const CLEAN_MSJ_ROLE = 'clean_msj_role';

export const GET_LISTACAMPO = 'get_listacampo';
export const ERROR_LISTACAMPO = 'error_listacampo';
export const CREATE_LISTACAMPO = 'create_listacampo';
export const UPDATE_LISTACAMPO = 'update_listacampo';
export const DELETE_LISTACAMPO = 'delete_listacampo';
export const CLEAN_MSJ_LISTACAMPO = 'clean_msj_listacampo';

export const GET_INSTITUCION = 'get_institucion';
export const ERROR_INSTITUCION = 'error_institucion';
export const CREATE_INSTITUCION = 'create_institucion';
export const UPDATE_INSTITUCION = 'update_institucion';
export const DELETE_INSTITUCION = 'delete_institucion';
export const CLEAN_MSJ_INSTITUCION = 'clean_msj_institucion';

export const GET_DISPOSITIVO = 'get_dispositivo';
export const ERROR_DISPOSITIVO = 'error_dispositivo';
export const CREATE_DISPOSITIVO = 'create_dispositivo';
export const UPDATE_DISPOSITIVO = 'update_dispositivo';
export const DELETE_DISPOSITIVO = 'delete_dispositivo';
export const CLEAN_MSJ_DISPOSITIVO = 'clean_msj_dispositivo';

export const GET_SENSOR = 'get_sensor';
export const ERROR_SENSOR = 'error_sensor';
export const CREATE_SENSOR = 'create_sensor';
export const UPDATE_SENSOR = 'update_sensor';
export const DELETE_SENSOR = 'delete_sensor';
export const CLEAN_MSJ_SENSOR = 'clean_msj_sensor';

export const GET_MEDICION = 'get_medicion';
export const ERROR_MEDICION = 'error_medicion';
export const CREATE_MEDICION = 'create_medicion';
export const UPDATE_MEDICION = 'update_medicion';
export const DELETE_MEDICION = 'delete_medicion';
export const CLEAN_MSJ_MEDICION = 'clean_msj_medicion';

export const GET_POSICION = 'get_posicion';
export const ERROR_POSICION = 'error_posicion';
export const CREATE_POSICION = 'create_posicion';
export const UPDATE_POSICION = 'update_posicion';
export const DELETE_POSICION = 'delete_posicion';
export const CLEAN_MSJ_POSICION = 'clean_msj_posicion';

export const GET_ROTACION = 'get_rotacion';
export const ERROR_ROTACION = 'error_rotacion';
export const CREATE_ROTACION = 'create_rotacion';
export const UPDATE_ROTACION = 'update_rotacion';
export const DELETE_ROTACION = 'delete_rotacion';
export const CLEAN_MSJ_ROTACION = 'clean_msj_rotacion';
