import axios from 'axios';
import History from '../history.js';
import {
    AUTH_USER,
    UNAUTH_USER,
    AUTH_ERROR,
    FETCH_FEATURE,
    AUTH_EXPIRED,
    AUTH_NOT_EXPIRED
} from './types';

import { API_AUTH } from '../routes/config'

export const signinUser = ({ username, password }) => {
    console.log(username)
    console.log(password)
    return (dispatch) => {
        // submit username/password to the server
        axios.post(`${API_AUTH}/signin`, { username, password })
            .then(response => {

                // if request is good...
                // - update state to indicate user is authenticated
                dispatch({ type: AUTH_USER });

                // - save the jwt token
                localStorage.setItem('token', response.data.token);
                localStorage.setItem('id', JSON.stringify(response.data.id));

                // - redirect to the route '/feature'
                History.push('/dashboard');

            }).catch(err => {   

                // if request is bad...
                // - show an error to the user
                console.log(err)
                dispatch(authError('Error'));
            });

        // fetch(`${API_AUTH}/signin`, {
        //     method: 'POST', // or 'PUT'
        //     body: JSON.stringify({ username, password }), // data can be `string` or {object}!
        //     headers: {
        //         'Content-Type': 'application/json'
        //     }
        // }).then(res => res.json())
        //     .catch(error => { dispatch(authError('Error')) })
        //     .then(response => {
        //         dispatch({ type: AUTH_USER });

        //         localStorage.setItem('token', response.token);
        //         localStorage.setItem('user', JSON.stringify(response.user));

        //         History.push('/dashboard');
        //     });
    };
};


export const authError = (error) => {
    return {
        type: AUTH_ERROR,
        payload: error
    };
};

export const signoutUser = () => {
    eliminarLocalStorage()
    History.push('/signin');
    return { type: UNAUTH_USER };
};

export const checkSession = () => {
    return (dispatch) => {
        axios.get(`${API_AUTH}/jwt`, {
            headers: { "x-access-token": localStorage.getItem('token') }
        })
            .then(response => {
                console.log(response)
                dispatch({ type: AUTH_NOT_EXPIRED });
            }).catch(err => {
                eliminarLocalStorage()
                dispatch({ type: AUTH_EXPIRED, payload: 'sesión expirada' });
            });
    };
};

const eliminarLocalStorage = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('user');
}
