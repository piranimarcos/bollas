import axios from 'axios';
// import History from '../history.js';
import {
    GET_SENSOR,
    ERROR_SENSOR,
    CREATE_SENSOR,
    UPDATE_SENSOR,
    CLEAN_MSJ_SENSOR,
    DELETE_SENSOR
} from './types';

import { API_V1 } from '../routes/config'


export const fetchSensors = () => {
    return (dispatch) => {
        axios.get(`${API_V1}/sensors`, {
            headers: { "x-access-token": localStorage.getItem('token') }
        })
            .then(response => {
                dispatch({
                    type: GET_SENSOR,
                    payload: response.data
                });
            })
            .catch(err => {
                dispatch(sensorError(err.message))
            })
    };
};


export const getSensor = (id) => {
    return (dispatch) => {
        axios.get(`${API_V1}/sensors?conditions={ "_id": "${id}" }`, {
            headers: { "x-access-token": localStorage.getItem('token') }
        })
            .then(response => {
                dispatch({
                    type: GET_SENSOR,
                    payload: response.data
                });
            })
            .catch(err => {
                dispatch(sensorError(err.message))
            })
    };
};


export const sensorError = (error) => {
    return {
        type: ERROR_SENSOR,
        payload: error
    };
};


export const clearMsj = () => {
    return {
        type: CLEAN_MSJ_SENSOR
    };
};
