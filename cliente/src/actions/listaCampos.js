import axios from 'axios';
// import History from '../history.js';
import {
    GET_LISTACAMPO,
    ERROR_LISTACAMPO,
    CREATE_LISTACAMPO,
    UPDATE_LISTACAMPO,
    CLEAN_MSJ_LISTACAMPO,
    DELETE_LISTACAMPO
} from './types';

import { API_V1 } from '../routes/config'


export const fetchListaCampos = () => {
    return (dispatch) => {
        axios.get(`${API_V1}/listaCampos`, {
            headers: { "x-access-token": localStorage.getItem('token') }
        })
            .then(response => {
                dispatch({
                    type: GET_LISTACAMPO,
                    payload: response.data
                });
            }).catch(err => {
                dispatch(listaCampoError(err.message))
            })
    };
};

export const crearListaCampo = (listaCampo) => {

    console.log(listaCampo);
    
    return (dispatch) => {
        dispatch(clearMsj())
        axios.post(`${API_V1}/listaCampos`, listaCampo, {
            headers: { "x-access-token": localStorage.getItem('token') }
        }).then(response => {
            dispatch({ type: CREATE_LISTACAMPO, payload: response.data.message });
            dispatch(fetchListaCampos())
        }).catch(err => {

            dispatch(listaCampoError(err.response.data.message))
        })
    };
};

export const modificarListaCampo = (listaCampo) => {
    return (dispatch) => {
        axios.put(`${API_V1}/listaCampos`, {
            listaCampo,
            headers: { 'x-access-token': localStorage.getItem('token') }
        })
            .then(response => {
                console.log(response);

                dispatch({ type: UPDATE_LISTACAMPO, payload: response.data.message });
            })
            .catch(err => {
                dispatch(listaCampoError(err.response.data.message))
            })
    };
};

export const borrarListaCampo = (id) => {
    
    return (dispatch) => {
        dispatch(clearMsj())
        axios.delete(`${API_V1}/listaCampos/${id}`, {
            headers: { "x-access-token": localStorage.getItem('token') }
        }).then(response => {
            console.log(response);
            
            dispatch({ type: DELETE_LISTACAMPO, payload: response.data.message });
            dispatch(fetchListaCampos())
        }).catch(err => {
            dispatch(listaCampoError(err.response.data.message))
        })
    };
};

export const listaCampoError = (error) => {
    return {
        type: ERROR_LISTACAMPO,
        payload: error
    };
};


export const clearMsj = () => {
    return {
        type: CLEAN_MSJ_LISTACAMPO
    };
};
