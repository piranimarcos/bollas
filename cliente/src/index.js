import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter, Route, Router, Switch, Redirect } from 'react-router-dom';
import History from './history.js';
import store from './store/createStore'

import DashboardRoutes from './routes';

import isAuth from './components/auth/is_auth';
import RequireAuth from './components/auth/require_auth';

import Signin from './components/auth/signin';
// import Signout from './components/auth/signout';
import Signup from './components/auth/signup';
import Feature from './components/feature';
import NotFound from './components/notfound';

import { AUTH_USER } from './actions/types';
import { checkSession } from './actions/';

import './assets/style/index.css';
// import * as serviceWorker from './serviceWorker';


if (process.env.NODE_ENV === 'production')
    console.log = () => {};

const token = localStorage.getItem('token');

// if we have a token, consiger the user to be signed in
if (token) {
    store.dispatch({ type: AUTH_USER });
    store.dispatch(checkSession());
}


ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <Router history={History}>

                <Switch>
                    <Route exact path="/" component={() => <Redirect from="/" to="/dashboard"/> } />
                    <Route exact path="/signin" component={isAuth(Signin)} />
                    <Route exact path="/feature" component={RequireAuth(Feature)} />


                    <DashboardRoutes />


                    <Route component={NotFound} />

                </Switch>

            </Router>
        </BrowserRouter>
    </Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();
