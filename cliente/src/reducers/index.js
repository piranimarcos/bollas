import { combineReducers } from 'redux';
import { reducer as authReducer } from './auth';
import { reducer as featureReducer } from './feature';
import { reducer as userReducer } from './user';
import { reducer as roleReducer } from './role';
import { reducer as institucionReducer } from './institucion';
import { reducer as dispositivoReducer } from './dispositivo';
import { reducer as sensorReducer } from './sensor';
import { reducer as medicionReducer } from './medicion';
import { reducer as posicionReducer } from './posicion';
import { reducer as rotacionReducer } from './rotacion';
import { reducer as listaCampoReducer } from './listaCampo';


import { reducer as formReducer } from 'redux-form';

const rootReducer = combineReducers({
    form: formReducer,
    auth: authReducer,
    user: userReducer,
    listaCampo: listaCampoReducer,
    role: roleReducer,
    institucion: institucionReducer,
    dispositivo: dispositivoReducer,
    sensor: sensorReducer,
    medicion: medicionReducer,
    posicion: posicionReducer,
    rotacion: rotacionReducer,
    features: featureReducer
});

export default rootReducer;