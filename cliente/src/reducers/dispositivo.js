import {
  GET_DISPOSITIVO,
  ERROR_DISPOSITIVO,
  CREATE_DISPOSITIVO,
  UPDATE_DISPOSITIVO,
  CLEAN_MSJ_DISPOSITIVO,
  DELETE_DISPOSITIVO
} from '../actions/types';

export const reducer = (state = {}, action) => {

  switch (action.type) {
    case GET_DISPOSITIVO:
      return { ...state, dispositivos: action.payload }
    case ERROR_DISPOSITIVO:
      return { ...state, message: '', error: action.payload }
    case CREATE_DISPOSITIVO:
    case UPDATE_DISPOSITIVO:
    case DELETE_DISPOSITIVO:
      return { ...state, message: action.payload, error: '' }
    case CLEAN_MSJ_DISPOSITIVO:
      return { ...state, message: '', error: '' }
    default:
      return state;
  }
};
