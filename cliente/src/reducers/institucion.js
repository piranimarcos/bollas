import {
  GET_INSTITUCION,
  ERROR_INSTITUCION,
  CREATE_INSTITUCION,
  UPDATE_INSTITUCION,
  CLEAN_MSJ_INSTITUCION,
  DELETE_INSTITUCION
} from '../actions/types';

export const reducer = (state = {}, action) => {

  switch (action.type) {
    case GET_INSTITUCION:
      return { ...state, instituciones: action.payload }
    case ERROR_INSTITUCION:
      return { ...state, message: '', error: action.payload }
    case CREATE_INSTITUCION:
    case UPDATE_INSTITUCION:
    case DELETE_INSTITUCION:
      return { ...state, message: action.payload, error: '' }
    case CLEAN_MSJ_INSTITUCION:
      return { ...state, message: '', error: '' }
    default:
      return state;
  }
};
