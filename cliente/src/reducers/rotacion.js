import {
  GET_ROTACION,
  ERROR_ROTACION,
  CREATE_ROTACION,
  UPDATE_ROTACION,
  CLEAN_MSJ_ROTACION,
  DELETE_ROTACION
} from '../actions/types';

export const reducer = (state = {}, action) => {

  switch (action.type) {
    case GET_ROTACION:
      return { ...state, rotacions: action.payload }
    case ERROR_ROTACION:
      return { ...state, message: '', error: action.payload }
    case CREATE_ROTACION:
    case UPDATE_ROTACION:
    case DELETE_ROTACION:
      return { ...state, message: action.payload, error: '' }
    case CLEAN_MSJ_ROTACION:
      return { ...state, message: '', error: '' }
    default:
      return state;
  }
};
