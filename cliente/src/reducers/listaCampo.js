import {
  GET_LISTACAMPO,
  ERROR_LISTACAMPO,
  CREATE_LISTACAMPO,
  UPDATE_LISTACAMPO,
  CLEAN_MSJ_LISTACAMPO,
  DELETE_LISTACAMPO
} from '../actions/types';

export const reducer = (state = {}, action) => {

  switch (action.type) {
    case GET_LISTACAMPO:
      return { ...state, listaCampos: action.payload }
    case ERROR_LISTACAMPO:
      return { ...state, message: '', error: action.payload }
    case CREATE_LISTACAMPO:
    case UPDATE_LISTACAMPO:
    case DELETE_LISTACAMPO:
      return { ...state, message: action.payload, error: '' }
    case CLEAN_MSJ_LISTACAMPO:
      return { ...state, message: '', error: '' }
    default:
      return state;
  }
};
