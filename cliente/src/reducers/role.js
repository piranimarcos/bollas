import {
  GET_ROLE,
  ERROR_ROLE,
  CREATE_ROLE,
  UPDATE_ROLE,
  CLEAN_MSJ_ROLE,
  DELETE_ROLE
} from '../actions/types';

export const reducer = (state = {}, action) => {

  switch (action.type) {
    case GET_ROLE:
      return { ...state, roles: action.payload }
    case ERROR_ROLE:
      return { ...state, message: '', error: action.payload }
    case CREATE_ROLE:
    case UPDATE_ROLE:
    case DELETE_ROLE:
      return { ...state, message: action.payload, error: '' }
    case CLEAN_MSJ_ROLE:
      return { ...state, message: '', error: '' }
    default:
      return state;
  }
};
