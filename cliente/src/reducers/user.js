import {
  GET_USER,
  ERROR_USER,
  CREATE_USER,
  UPDATE_USER,
  CLEAN_MSJ,
  DELETE_USER
} from '../actions/types';

export const reducer = (state = {}, action) => {

  switch (action.type) {
    case GET_USER:
      return { ...state, users: action.payload }
    case ERROR_USER:
      return { ...state, message: '', error: action.payload }
    case CREATE_USER:
    case UPDATE_USER:
    case DELETE_USER:
      return { ...state, message: action.payload, error: '' }
    case CLEAN_MSJ:
      return { ...state, message: '', error: '' }
    default:
      return state;
  }
};
