import {
  GET_MEDICION,
  ERROR_MEDICION,
  CREATE_MEDICION,
  UPDATE_MEDICION,
  CLEAN_MSJ_MEDICION,
  DELETE_MEDICION
} from '../actions/types';

export const reducer = (state = {}, action) => {

  switch (action.type) {
    case GET_MEDICION:
      return { ...state, medicions: action.payload }
    case ERROR_MEDICION:
      return { ...state, message: '', error: action.payload }
    case CREATE_MEDICION:
    case UPDATE_MEDICION:
    case DELETE_MEDICION:
      return { ...state, message: action.payload, error: '' }
    case CLEAN_MSJ_MEDICION:
      return { ...state, medicions: [], message: '', error: '' }
    default:
      return state;
  }
};
