import {
    AUTH_USER,
    UNAUTH_USER,
    AUTH_ERROR,
    AUTH_EXPIRED,
    AUTH_NOT_EXPIRED
} from '../actions/types';

export const reducer = (state = {}, action) => {

    switch (action.type) {
        case AUTH_USER:
        case AUTH_NOT_EXPIRED:
            return { ...state, error: '', authenticated: true, expired: ''}
        case UNAUTH_USER:
            return { ...state, authenticated: false }
        case AUTH_ERROR:
            return { ...state, error: action.payload }
        case AUTH_EXPIRED:
            return { ...state, authenticated: false, expired: action.payload }
        default:
            return state;
    }
};
