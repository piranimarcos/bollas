import {
  GET_POSICION,
  ERROR_POSICION,
  CREATE_POSICION,
  UPDATE_POSICION,
  CLEAN_MSJ_POSICION,
  DELETE_POSICION
} from '../actions/types';

export const reducer = (state = {}, action) => {

  switch (action.type) {
    case GET_POSICION:
      return { ...state, posicions: action.payload }
    case ERROR_POSICION:
      return { ...state, message: '', error: action.payload }
    case CREATE_POSICION:
    case UPDATE_POSICION:
    case DELETE_POSICION:
      return { ...state, message: action.payload, error: '' }
    case CLEAN_MSJ_POSICION:
      return { ...state, message: '', error: '' }
    default:
      return state;
  }
};
