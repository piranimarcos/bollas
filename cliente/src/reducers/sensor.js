import {
  GET_SENSOR,
  ERROR_SENSOR,
  CREATE_SENSOR,
  UPDATE_SENSOR,
  CLEAN_MSJ_SENSOR,
  DELETE_SENSOR
} from '../actions/types';

export const reducer = (state = {}, action) => {

  switch (action.type) {
    case GET_SENSOR:
      return { ...state, sensors: action.payload }
    case ERROR_SENSOR:
      return { ...state, message: '', error: action.payload }
    case CREATE_SENSOR:
    case UPDATE_SENSOR:
    case DELETE_SENSOR:
      return { ...state, message: action.payload, error: '' }
    case CLEAN_MSJ_SENSOR:
      return { ...state, message: '', error: '' }
    default:
      return state;
  }
};
