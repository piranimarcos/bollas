import React from 'react';
import { Route, Switch } from 'react-router-dom';
import App from '../components/App';
import Welcome from '../components/welcome';
import NotFound from '../components/notfound';

import Dispositivos from '../components/Dispositivos';
import Sensores from '../components/Sensores';

import RequireAuth from '../components/auth/require_auth';
import Mediciones from '../components/Mediciones';
import Galeria from '../components/Galeria';
import Posiciones from '../components/Posiciones';
import Rotaciones from '../components/Rotaciones';
import Dashboard from '../components/Dashboard';

const Routes = () => {
    return (
        <App>

            <Switch>

                {/* <Route exact path="/dashboard/" component={RequireAuth(Welcome)} /> */}
                <Route exact path="/dashboard/" component={RequireAuth(Dashboard)} />
                <Route exact path="/dashboard/dispositivos" component={RequireAuth(Dispositivos)} />
                <Route exact path="/dashboard/dispositivos/:dispositivo" component={RequireAuth(Posiciones)} />
                <Route exact path="/dashboard/dispositivos/rotacion/:dispositivo" component={RequireAuth(Rotaciones)} />
                <Route exact path="/dashboard/sensores" component={RequireAuth(Sensores)} />
                <Route exact path="/dashboard/sensores/:sensor" component={RequireAuth(Mediciones)} />
                <Route exact path="/dashboard/galeria" component={RequireAuth(Galeria)} />

                <Route component={RequireAuth(NotFound)} />

            </Switch>

        </App>
    );
};

export default Routes;
