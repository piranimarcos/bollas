let SERVER = 'http://localhost:4000/'

if(process.env.NODE_ENV === 'production'){
  SERVER = '/';
}

export const API_AUTH = `${SERVER}api/auth`;

export const API_V1 = `${SERVER}api/v1`;

export const FOTOS = SERVER

export const SOCKET = SERVER