const express = require("express");
const bodyParser = require("body-parser");
const path = require('path');
const cors = require("cors");
const mongoose = require('mongoose');
const baucis = require('baucis')
const Dispositivo = require('./models/Dispositivo');
const Posicion = require('./models/Posicion');
const Rotacion = require('./models/Rotacion');
const Sensor = require('./models/Sensor');
const Evento = require('./models/Evento');
const Medicion = require('./models/Medicion');
const User = require('./models/User');

const CarpetaFotos = `${process.env.HOME}/fotos/Capturas/`;
const fs = require('fs');
const resize = require('./utils/resize')

const dispositivo = require("./routes/dispositivo.routes");


const app = express()
const server = require('http').Server(app)
const io = require('socket.io')(server)



baucis.rest('dispositivo')
baucis.rest('posicion')
baucis.rest('rotacion')
baucis.rest('medicion')
baucis.rest('evento')
baucis.rest('sensor')
baucis.rest('user')


mongoose
	.connect(
		'mongodb://localhost/bollas',
		{ useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, },
	)
	.then(() => console.log("MongoDB successfully connected"))
	.catch(err => console.log(err));


// const corsOptions = {
// 	// origin: "http://localhost:3000"
// 	origin: "*"
// };

// app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

app.use((req, res, next) => {
	fecha = new Date()
	console.log("--------------------------");
	console.log('Request Url:', req.url);
	console.log('Request Type:', req.method);
	console.log('Request Body:', req.body);
	console.log('Request Header:', req.header);
	console.log("Time: " + fecha.getDate() + "/" + (fecha.getMonth() + 1) + " - " + fecha.getHours() + ":" + fecha.getMinutes() + ":" + fecha.getSeconds());

	next();
})


//cors
app.use(function (req, res, next) {
	res.header("Access-Control-Allow-Headers", "x-access-token, Origin, Content-Type, Accept");
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');

	next();
});


// routes

app.get('/fotos', (req, res) => {
	const widthString = req.query.width
	const heightString = req.query.height
	const format = req.query.format
	const img = req.query.img

	// Parse to integer if possible
	let width, height
	if (widthString) {
		width = parseInt(widthString)
	}
	if (heightString) {
		height = parseInt(heightString)
	}
	// Set the content-type of the response
	res.type(`image/${format || 'png'}`)

	// Get the resized image
	resize(CarpetaFotos + img, format, width).pipe(res)
	// res.sendFile(path.join(CarpetaFotos, img));
})


app.get('/fotos-nombre', (req, res) => {
	fs.readdir(CarpetaFotos, (err, files) => {
		let nombres = []
		files.forEach(file => {
			nombres.push({ nombre: file })
		});
		res.json(nombres);
	});
})

app.use('/api/v1', baucis());

app.use("/api/v1", dispositivo);


require('./routes/auth.routes')(app);

//defino esto para servir react en produccion
if (process.env.NODE_BOLLAS == 'prod') {
	app.use(express.static(path.join(__dirname, 'cliente/build/')));
	app.get('/*', function (req, res) {
		res.sendFile(path.join(__dirname, 'cliente/build/', 'index.html'));
	});
} else {
	app.get("/", (req, res) => {
		res.json({ message: "bienvenido a la aplicacion del chino." });
	});
}

// socket io

let connections = 0

io.sockets.on('connection', (socket) => {
	console.log('Usuarios conectados --> ' + ++connections)
	socket.on('disconnect', () => {
		console.log('Usuarios conectados --> ' + --connections)
	})
})


//escucha de eventos en el servidor tcp

const emisorEvento = require('./tcp')

emisorEvento.on('nuevaMedicion', function (e, nombre) {
	console.log('nuevaMedicion =>' + e);
	io.emit('nuevaMedicion', e, nombre);
});

emisorEvento.on('nuevaAlertaMedicion', function (e, nombre) {
	console.log('nuevaAlertaMedicion =>' + e);
	io.emit('nuevaAlertaMedicion', e, nombre);
});

emisorEvento.on('nuevaPosicion', function (e) {
	console.log('nuevaPosicion =>' + e);
	io.emit('nuevaPosicion', e);
});

emisorEvento.on('nuevaAlertaPosicion', function (e, nombre) {
	console.log('nuevaAlertaPosicion =>' + e);
	io.emit('nuevaAlertaPosicion', e, nombre)
});

emisorEvento.on('nuevaRotacion', function (e) {
	console.log('nuevaRotacion =>' + e);
	io.emit('nuevaRotacion', e);
});




const PORT = process.env.PORT || (process.env.NODE_BOLLAS == 'prod') ? 3000 : 4000;

// set port, listen for requests
server.listen(PORT, () => {
	console.log(`Servidor corriendo en el puerto ${PORT}.`);
});
console.log('Node Env: ' + process.env.NODE_BOLLAS);

