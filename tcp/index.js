// Include Nodejs' net module.
const Net = require('net');
const events = require('events');
const mongoose = require('mongoose');
const Dispositivo = require('../models/Dispositivo');
const Sensor = require('../models/Sensor');
const Evento = require('../models/Evento');
const Posicion = require('../models/Posicion');
const Rotacion = require('../models/Rotacion');
const Medicion = require('../models/Medicion');
const { isBoolean } = require('util');

// The port on which the server is listening.
const port = 9000;
const server = new Net.Server();

const emisorEvento = new events.EventEmitter();

mongoose.connect('mongodb://localhost/bollas', { useUnifiedTopology: true, useNewUrlParser: true });
mongoose.connection.once('open', function () {
    console.log('Conectado con la base de datos MongoDB.');
});
mongoose.connection.on('error', function () {
    console.log('Error al conectar con MongoDB, asegurese de que la db esta corriendo.');
});

server.listen(port, function () {
    console.log(`Servidor TCP escuchando en el puerto: ${port}.`);
});


server.on('connection', conexion);

const enviarAlertaMedicion = function  (med, nombre) {
    emisorEvento.emit('nuevaAlertaMedicion', JSON.stringify(med), nombre)
}

const enviarAlertaPosicion = function (med, dispositivo) {
    emisorEvento.emit('nuevaAlertaPosicion', JSON.stringify(med), dispositivo)
}

const enviarMedicion = function (med, nombre) {
    emisorEvento.emit('nuevaMedicion', JSON.stringify(med), nombre)
}

const enviarPosicion = function (med) {
    emisorEvento.emit('nuevaPosicion', JSON.stringify(med))
}

const enviarRotacion = function (med) {
    emisorEvento.emit('nuevaRotacion', JSON.stringify(med))
}

const esAlerta = function (valor, condiciones, tipo) {
    let alerta = false;
    //digital
    if (tipo === 'digital') {
        if (condiciones[0].valor == valor) {
            alerta = true;
        }
    }

    // analogico
    if (tipo === 'analogico') {
        condiciones.forEach(function (cond) {
            if ((cond.condicion === '>') && (valor > parseFloat(cond.valor))) {
                alerta = true;
            }
            if ((cond.condicion === '<') && (valor < parseFloat(cond.valor))) {
                alerta = true;
            }
            if ((cond.condicion === '=') && (valor === parseFloat(cond.valor))) {
                alerta = true;
            }
        });
    }
    return alerta;
}


const alertaPos = function (distancia, distanciaCritica) {
    let alerta = false;

    // analogico
    if (distancia > distanciaCritica) {
        alerta = true
    }

    return alerta;
}


const getKilometros = function (lat1, lon1, lat2, lon2) {
    rad = function (x) { return x * Math.PI / 180; }
    let R = 6378.137; //Radio de la tierra en km
    let dLat = rad(lat2 - lat1);
    let dLong = rad(lon2 - lon1);
    let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(rad(lat1)) * Math.cos(rad(lat2)) * Math.sin(dLong / 2) * Math.sin(dLong / 2);
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    let d = R * c;
    return d.toFixed(3); //Retorna tres decimales
}



function conexion(conn) {
    let remoteAddres = `${conn.remoteAddress}:${conn.remotePort}`
    console.log(`cliente nuevo:  ${remoteAddres}`);

    conn.setEncoding('utf8');

    conn.on('data', reciboDatos);
    conn.once('close', cierroConexion);
    conn.on('error', errorConexion);



    function reciboDatos(d) {
        try {
            datosJson = JSON.parse(d);
        } catch (e) {
            console.log('DEBE ENVIAR UN OBJETO JSON \n');
            conn.end('DEBE ENVIAR UN OBJETO JSON \n');
            return 0;
        }

        if (!(datosJson.llave)) {
            console.log('LLAVE NO ENVIADA \n');
            conn.end('LLAVE NO ENVIADA \n');
        } else {
            console.log(`PUEDO BUSCAR DISPOSITIVO CON LA LLAVE: ${datosJson.llave}\n`);

            Dispositivo.findOne({ llave: datosJson.llave })
                .populate({
                    path: 'eventos',
                    populate: { path: 'idSensor' }
                })
                .exec(function (err, disp) {

                    if (err) { conn.end('PROBLEMA EN LA DB DISP \n'); return console.error(err); }

                    if (!disp) { conn.end('NO HAY DISPOSITIVO \n'); return console.error('no hay dispositivo'); }

                    let eventos = disp.eventos
                    console.log(disp);

                    Sensor.find({ idDispositivo: disp._id }, function (err, sensores) {
                        if (err) { conn.end('PROBLEMA EN LA DB SENS \n'); return console.error(err); }

                        // console.log(sens); 
                        sensores.map(function (sensor) {
                            console.log(sensor.nombreRef);

                            for (let key in datosJson) {
                                if (sensor.nombreRef == key) {
                                    console.log(`se encontro sensor: ${sensor.nombre}`);

                                    let medicion = new Medicion({
                                        idSensor: sensor._id,
                                        valor: datosJson[key],
                                        alerta: esAlerta(datosJson[key], sensor.condiciones, sensor.tipo)
                                    });

                                    medicion.save(function (err, med) {
                                        if (err) { conn.end('PROBLEMA EN LA DB MED \n'); return console.error(err); }
                                        console.log(sensor.nombre + " --->  medicion guardada.");
                                        enviarMedicion(med, sensor.nombre)
                                        if(med.alerta){
                                            enviarAlertaMedicion(med, sensor.nombre)
                                        }
                                        
                                    })

                                }
                            }
                        })

                        let pos = {}
                        if (("lat" in datosJson) && ("lng" in datosJson)) {
                            pos.lat = datosJson["lat"]
                            pos.lng = datosJson["lng"]

                            let dist = getKilometros(disp.posicion.lat, disp.posicion.lng, pos.lat, pos.lng)

                            let posicion = new Posicion({
                                idDispositivo: disp._id,
                                lat: pos.lat,
                                lng: pos.lng,
                                distancia: dist,
                                alerta: alertaPos(dist, disp.distanciaCritica)
                            });

                            posicion.save(function (err, med) {
                                if (err) { conn.end('PROBLEMA EN LA DB MED \n'); return console.error(err); }
                                console.log(pos.lat + " - " + pos.lng + " --->  posicion guardada.");
                                med['nombreSocket'] = 'Posicion'
                                enviarPosicion(med)
                                if(med.alerta){
                                    enviarAlertaPosicion(med, disp.nombre)
                                }
                            })

                        }

                        let rot = {}
                        if (("rotacionX" in datosJson) && ("rotacionY" in datosJson)) {
                            rot.x = datosJson["rotacionX"]
                            rot.y = datosJson["rotacionY"]

                            let rotacion = new Rotacion({
                                idDispositivo: disp._id,
                                x: rot.x,
                                y: rot.y
                            });

                            rotacion.save(function (err, med) {
                                if (err) { conn.end('PROBLEMA EN LA DB MED \n'); return console.error(err); }
                                console.log(" --->  rotacion guardada.");
                                med['nombreSocket'] = 'Oleaje'
                                enviarRotacion(med)
                            })

                        }

                        conn.end('OK \n');
                        console.log("OK");
                    })

                })
        }
    }

    function cierroConexion() {
        console.log(`connection from ${remoteAddres} closed'`);
    }

    function errorConexion(err) {
        console.log(`Connection ${remoteAddres} error:${err.message}`);
        conn.write(err);
    }
}

module.exports = emisorEvento