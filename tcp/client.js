const net = require('net');

const client = new net.Socket();
client.connect(9000, '127.0.0.1', function () {
// client.connect(9000, '138.97.201.120', function () {
    console.log('Connected');

    // client.write(`{"llave": "123123","voltajeBateriaGps":"${Math.round(Math.random() * 10)}","rotacionY": "${Math.round(Math.random() * 10)}"}`);
    client.write(`{"llave": "123123","voltajeBateriaGps":"${Math.round(Math.random() * 10)}","rotacionY": "${Math.round(Math.random() * 10)}"}`);

}); 

client.on('data', function (data) {
    console.log('Received: ' + data);
    client.destroy(); // kill client after server's response
});

client.on('close', function () {
    console.log('Connection closed');
});